package thesis.travelspeakbook.current_data;

import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import model.Category;
import model.ConvertUnsigned;
import model.DatabaseTravelSpeakBook;
import model.Item;
import model.Item.Language;
import model.MySentence;
import thesis.travelspeakbook.R;
import android.app.Activity;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

public class Constant {
	public final static float THRESHOLD_MIN_PERCENT = 0.5f;
	
	public final static String[] TAGS_USE_NUM_KEYBOARD = new String[]{"num", "age", "phone", "hour"};
	public final static String[] TAGS_USE_DATE_KEYBOARD = new String[]{"date"};
	public final static String[] TAGS_USE_TEXT_KEYBOARD = new String[]{"text", "place", "food"};
	
	public final static String STORE_CATEGORY = "category";
	public final static String TRAVEL_STORAGE = "TRAVEL_STORAGE";
	public final static String STORE_LANGUAGE = "store_language_input";
	public final static String STORE_LANGUAGE_OUTPUT = "store_language_output";
	public final static String STORE_IS_FIRST_USE = "is_first_use";
	
	public final static int STORE_NON_CHOSEN_LANG = 99;
	
	
	
	public final static String NON_TEXT_DEFAULT = "Non text";
	public final static String NON_TEXT_TRANSLATE_DEFAULT = "Non text translate";
	
	public static boolean isInputLang;
	// TEST
//	public final static String test= "test";
//	public final static String testanother 
	
	// FOR COMMON CATE
	public final static String COMMON_BASIC = "Basic";
	public final static String COMMON_BASIC_ASK_PRICE = "Ask price";
	
	// TODO: Remove these tag constants
	
	static Language LANGMODE_INPUT = Language.VIET;
	static Language LANGMODE_OUTPUT = Language.ENG;
	static Language LANGMODE_SEARCH = Language.VIET_UNSIGNED;
	

	public static String CURRENT_CONTEXT_SHOW_FRONT_END = "";
	public static MySentence CURRENT_SENTENCE;
	public static Activity CURRENT_ACTIVITY;
	
	public static Category CURRENT_CATE;
	public static Category CURRENT_ROOT_CATE;
	public static Category CATE_FAVORITE;
	public static DatabaseTravelSpeakBook DATABASE;
	
	
	
	

	public static String DATABASE_SDCARD = "database_sdcard20";
	public static char SIGN_FAVORITE = '*';
	
	// 
	public final static String HEADER_HTML = "<font color=\"#01bef5\">";
	public final static String TAIL_HTML = "</font>";
	public static final String BOLD_HEADER = "<b>";
	public static final String BOLD_TAIL = "</b>";
	
	public final static String DATE_DEFAULT = HEADER_HTML + "1/1/1991" + TAIL_HTML;
	public final static String NUM_DEFAULT = HEADER_HTML + "1" + TAIL_HTML;
	public final static String NAME_DEFAULT = HEADER_HTML + "Alex" + TAIL_HTML;
	public final static String TIME_DEFAULT = HEADER_HTML + "7:00" + TAIL_HTML;
	public final static String PLACE_DEFAULT = HEADER_HTML + "location" + TAIL_HTML;
	public final static String TAG_SHOW_DEFAULT = "...";
	
	
	// resource tieng anh va tieng viet
	public static String[] RES_SHORTHAND_LANG = new String[] {"ENG", "VIE", "LAO", "CAM"};
	public static String[] RES_CLEAR = new String[] {"clear", "xóa", "clear", "clear"};
	public static String[] RES_TODAY = new String[] {"today", "hôm nay", "today", "today"};
	public static String[] RES_YESTERDAY = new String[] {"yesterday", "hôm qua", "yesterday", "yesterday"};
	public static String[] RES_TOMORROW = new String[] {"tomorrow", "ngày mai", "tomorrow", "tomorrow"};
	public static String[] RES_BACK_LIST = new String[] {"\u25C0 sentences list", "\u25C0 danh sách câu", "\u25C0 sentence list", "\u25C0 sentence list"};
	public static String[] RES_TYPE_SEARCH = new String[] {"TYPE YOUR SENTENCE HERE...", "NHẬP CÂU CỦA BẠN VÀO ĐÂY", "TYPE YOUR SENTENCE HERE...", "TYPE YOUR SENTENCE HERE..."};
	public static String[] RES_SEARCH = new String[] {"Search", "Tìm kiếm", "Search", "Search"};
	public static String[] RES_COMMAND = new String[] {"FIND YOUR SENTENCE BY QUICKLY SEARCH OR CHOOSING AN APPROPRIATE CONTEXT", "TÌM CÂU BẰNG CÁCH DÙNG THANH SEARCH HOẶC CHỌN NGỮ CẢNH PHÙ HỢP", "FIND YOUR SENTENCE BY QUICKLY SEARCH OR CHOOSING AN APPROPRIATE CONTEXT", "FIND YOUR SENTENCE BY QUICKLY SEARCH OR CHOOSING AN APPROPRIATE CONTEXT"};
	public static String[] RES_SPEAK = new String[] {"SPEAK!", "NÓI!", "SPEAK!", "SPEAK!"};
//	public static String[] langShortHand = new String[] {"ENGLISH", "VIETNAMESE"};
//	
	

	public static int[] RES_FLAG_SMALL_ON = new int[] {R.drawable.eng_on, R.drawable.viet_on, R.drawable.lao_on, R.drawable.cam_on};
	public static void setLangMode(Language langInput, Language langOutput) {
		LANGMODE_INPUT = langInput;
		LANGMODE_OUTPUT = langOutput;
		if (LANGMODE_INPUT == Language.VIET) {
			LANGMODE_SEARCH = Language.VIET_UNSIGNED;
		}
		else
			LANGMODE_SEARCH = langInput;
	}
	
	static public String decorTextTagInput(String textTag) {
		return "<font color=\"#01bef5\">" + textTag + "</font>"; 
	}
	static public String decorTextTagOptinal(String textTag) {
//		return "<font color=\"#61fff5\">" + textTag + "</font>"; 
		return "<font color=\"#01bef5\">" + textTag + "</font>"; 
	}
	// passing intent data
	public final static String EXTRA_CATEGORY_ROOT = "mCateRoot";
	
	public static void showFragment(FragmentManager fragmentManager, Fragment f, int resID, boolean isKeepHistory) {
		FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
		fragmentTransaction.replace(resID, f);
		if (isKeepHistory)
			fragmentTransaction.addToBackStack(null);
		fragmentTransaction.commit();
	}
	static public HashMap<Category, Integer> RES_ID_BIG_ICON_CATE, RES_ID_MIDDLE_ICON_CATE, RES_ID_SMALL_ICON_CATE, RES_ID_SIDEBAR_CATE;
	static public int[] resIDBigIconCategory = {
		R.drawable.select_cate_favorite,
//		R.drawable.select_cate_all,
		R.drawable.select_cate_basic_phrases,
			R.drawable.select_cate_number,
			R.drawable.select_cate_timedate,
			R.drawable.select_cate_weather,
			R.drawable.select_cate_bordercrossing,
			R.drawable.select_cate_transport,
			R.drawable.select_cate_airport,
			R.drawable.select_cate_carrent,
			R.drawable.select_cate_directions,
			R.drawable.select_cate_accomodation,
			R.drawable.select_cate_communication,
			R.drawable.select_cate_sightseeing,
			R.drawable.select_cate_shopping,
			R.drawable.select_cate_entertainment,
			R.drawable.select_cate_fooddrink,
			R.drawable.select_cate_emergency,
			R.drawable.select_cate_health,
	};
	static public int[] resIDSmallIconCategory = {
		R.drawable.category_favorite_small,
//		R.drawable.select_cate_all,
		R.drawable.category_basic_phrases_small,
			R.drawable.category_number_small,
			R.drawable.category_timedate_small,
			R.drawable.category_weather_small,
			R.drawable.category_bordercrossing_small,
			R.drawable.category_transport_small,
			R.drawable.category_airport_small,
			R.drawable.category_carrent_small,
			R.drawable.category_direction_small,
			R.drawable.category_accommodation_small,
			R.drawable.category_communication_small,
			R.drawable.category_sightseeing_small,
			R.drawable.category_shopping_small,
			R.drawable.category_entertainment_small,
			R.drawable.category_fooddrink_small,
			R.drawable.category_emergency_small,
			R.drawable.category_health_small,};
	static public int[] resIDMiddleIconCategory= {
		R.drawable.category_fav_icon,
		R.drawable.category_basic_phrases_icon,
		R.drawable.category_num_icon,
		R.drawable.category_timedate_icon,
		R.drawable.category_weather_icon,
		R.drawable.category_bordercrossing_icon,
		R.drawable.category_transport_icon,
		R.drawable.category_airport_icon,
		R.drawable.category_carrent_icon,
		R.drawable.category_direction_icon,
		R.drawable.category_hotel_icon,
		R.drawable.category_communication_icon,
		R.drawable.category_sightseeing_icon,
		R.drawable.category_shopping_icon,
		R.drawable.category_entertainment_icon,
		R.drawable.category_fooddrink_icon,
		R.drawable.category_emergency_icon,
		R.drawable.category_health_icon		
	};
	
	static public int[] resIDSideBarIconCategory = {
		R.drawable.select_sidebar_cate_fav,
		R.drawable.select_sidebar_cate_basic_pharse,
		R.drawable.select_sidebar_cate_number,
		R.drawable.select_sidebar_cate_timedate,
		R.drawable.select_sidebar_cate_weather,
		R.drawable.select_sidebar_cate_bordercrossing,
		R.drawable.select_sidebar_cate_transport,
		R.drawable.select_sidebar_cate_airport,
		R.drawable.select_sidebar_cate_carrent,
		R.drawable.select_sidebar_cate_direction,
		R.drawable.select_sidebar_cate_accommodation,
		R.drawable.select_sidebar_cate_communication,
		R.drawable.select_sidebar_cate_sightseeing,
		R.drawable.select_sidebar_cate_shopping,
		R.drawable.select_sidebar_cate_entertainment,
		R.drawable.select_sidebar_cate_fooddrink,
		R.drawable.select_sidebar_cate_emergency,
		R.drawable.select_sidebar_cate_health,
	};
//	public static class IconSideBar {
//		int mOn;
//		int mOff;
//		public IconSideBar(int on, int off) {
//			this.mOn = on;
//			this.mOff = off;
//		}
//		public int getmOn() {
//			return mOn;
//		}
//		public void setmOn(int mOn) {
//			this.mOn = mOn;
//		}
//		public int getmOff() {
//			return mOff;
//		}
//		public void setmOff(int mOff) {
//			this.mOff = mOff;
//		}
//		
//	}
//	static public IconSideBar[] resIDSideBarCategory = {new IconSideBar(R.drawable.favorite_button_on, R.drawable.favorite_button_off),
//		new IconSideBar(R.drawable.basic_phrases_button_on, R.drawable.basic_phrases_button_off),
//		new IconSideBar(R.drawable.number_button_on, R.drawable.number_button_off),
//		new IconSideBar(R.drawable.timedate_button_on, R.drawable.timedate_button_off),
//		new IconSideBar(R.drawable.weather_button_on, R.drawable.weather_button_off),
//		new IconSideBar(R.drawable.bordercrossing_button_on, R.drawable.bordercrossing_button_off),
//		new IconSideBar(R.drawable.transport_button_on, R.drawable.transport_button_off),
//		new IconSideBar(R.drawable.airport_button_on, R.drawable.airport_button_off),
//		new IconSideBar(R.drawable.carrent_button_on, R.drawable.carrent_button_off),
//		new IconSideBar(R.drawable.direction_button_on, R.drawable.direction_button_off),
//		new IconSideBar(R.drawable.accommodation_button_on, R.drawable.accommodation_button_off),
//		new IconSideBar(R.drawable.communication_button_on, R.drawable.communication_button_off),
//		new IconSideBar(R.drawable.sightseeing_button_on, R.drawable.sightseeing_button_off),
//		new IconSideBar(R.drawable.shopping_button_on, R.drawable.shopping_button_off),
//		new IconSideBar(R.drawable.entertainment_button_on, R.drawable.entertainment_button_off),
//		new IconSideBar(R.drawable.fooddrink_button_on, R.drawable.fooddrink_button_off),
//		new IconSideBar(R.drawable.emergency_button_on, R.drawable.emergency_button_off),
//		new IconSideBar(R.drawable.health_button_on, R.drawable.health_button_off),
//	};
	
	static public int getValueLangSearch(String input) {
		// check input if tieng viet thi check xem tieng viet co dau hay khong dau
		int valueLangSearch = Constant.getLANGMODE_SEARCH().value;
		if (Constant.getLANGMODE_INPUT() == Language.VIET) {
			// check whether signed or not
			if (ConvertUnsigned.isSignedString(input))
				valueLangSearch = Constant.getLANGMODE_INPUT().value;
			else
				valueLangSearch = Constant.getLANGMODE_SEARCH().value;
		}
		return valueLangSearch;
	}
	
	public static Item[] commonWord = new Item[] {new Item(new String[] {"I", "tôi", "", ""}),
			new Item(new String[] {"is", "là", "", ""}),
			new Item(new String[] {"i", "không", "", ""}),
			new Item(new String[] {"this", "này", "", ""}),
			new Item(new String[] {"want", "muốn", "", ""}),
			new Item(new String[] {"nearest", "gần nhất", "", ""}),
			new Item(new String[] {"you", "bạn", "", ""}),
	};
	
	static public String pre_processing_input(String input) {
		input = input.replace("'ll", " will");
		input = input.replace("'d", " would");
		input = input.replace("'m", " am");
		input = input.replace("'s", " is");
		input = input.replace("'re", " are");
		input = input.replace("n't", " not");

		Pattern regex = Pattern.compile("[$&+,:;=?@#|*]");
		Matcher matcher = regex.matcher(input);
		if (matcher.find()){
			input = matcher.replaceAll("");
		}
		
		return input.trim();
	}

	// TODO: improve for get id cate faster
	/**
	 * @return the lANGMODE_INPUT
	 */
	public static Language getLANGMODE_INPUT() {
		return LANGMODE_INPUT;
	}

	/**
	 * @return the lANGMODE_OUTPUT
	 */
	public static Language getLANGMODE_OUTPUT() {
		return LANGMODE_OUTPUT;
	}

	/**
	 * @return the lANGMODE_SEARCH
	 */
	public static Language getLANGMODE_SEARCH() {
		return LANGMODE_SEARCH;
	}
}
