package thesis.travelspeakbook.adapter;

import model.Category;
import thesis.travelspeakbook.R;
import thesis.travelspeakbook.current_data.Constant;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class CategoryIconAdapter extends BaseAdapter {
	private Context context;
	private Category mCategory;

	public CategoryIconAdapter(Context context, Category category) {
		this.context = context;
		this.mCategory = category;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		View gridView;

		if (convertView == null) {
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			gridView = new View(context);
			gridView = inflater.inflate(R.layout.one_category, null);

		} else {
			gridView = (View) convertView;
		}

		TextView tvLabel = (TextView) gridView.findViewById(R.id.textViewLabel);
		Category childCate = ((Category) mCategory.get(position));
		tvLabel.setText(childCate.getText(Constant.getLANGMODE_INPUT().value));

		ImageView ivIcon = (ImageView) gridView
				.findViewById(R.id.imageViewIcon);

		ivIcon.setImageResource(Constant.RES_ID_BIG_ICON_CATE.get(childCate));

		return gridView;	
	}

	public int getCount() {
		return mCategory.size();
	}

	public Category getItem(int position) {
		return (Category) mCategory.get(position);
	}

	public long getItemId(int position) {
		return 0;
	}

}
