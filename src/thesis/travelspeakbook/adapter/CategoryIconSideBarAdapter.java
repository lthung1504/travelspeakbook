package thesis.travelspeakbook.adapter;

import model.Category;
import thesis.travelspeakbook.R;
import thesis.travelspeakbook.current_data.Constant;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

public class CategoryIconSideBarAdapter extends BaseAdapter{
	private Context context;
	private Category mCategory;
	
	public CategoryIconSideBarAdapter(Context context, Category category) {
		this.context = context;
		this.mCategory = category;
	}
	
	
	public View getView(int position, View convertView, ViewGroup parent) {
 
		LayoutInflater inflater = (LayoutInflater) context
			.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
 
		View gridView;
 
		if (convertView == null) {
			gridView = new View(context);
			gridView = inflater.inflate(R.layout.one_category_sidebar, null);
		} else {
			gridView = (View) convertView;
		}

		Category childCate = ((Category)mCategory.get(position));
//		tvLabel.setText(childCate.getText(Constant.getLANGMODE_INPUT().value));

		ImageView ivIcon = (ImageView) gridView
				.findViewById(R.id.imageViewIcon);

		ivIcon.setImageResource(Constant.RES_ID_SIDEBAR_CATE.get(childCate));
		return gridView;
	}
 
	
	public int getCount() {
		return mCategory.size();
	}
 
	
	public Category getItem(int position) {
		return (Category)mCategory.get(position);
	}
 
	
	public long getItemId(int position) {
		return 0;
	}

	
 
}
