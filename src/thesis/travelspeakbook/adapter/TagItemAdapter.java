package thesis.travelspeakbook.adapter;

import model.Item;
import model.TagSentence;
import thesis.travelspeakbook.R;
import thesis.travelspeakbook.current_data.Constant;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class TagItemAdapter extends BaseAdapter{
	private Context context;
	private TagSentence mTag;
	private Item mSelectedTagItem;
	
	public TagItemAdapter(Context context, TagSentence category, Item seletedTag) {
		this.context = context;
		this.mTag = category;
		mSelectedTagItem = seletedTag;
	}
	
	
	public View getView(int position, View convertView, ViewGroup parent) {
 
		LayoutInflater inflater = (LayoutInflater) context
			.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
 
		View gridView;
 
		if (convertView == null) {
			gridView = new View(context);
			gridView = inflater.inflate(R.layout.one_tag, null);
 
		} else {
			gridView = (View) convertView;
		}

		TextView tvTag = (TextView) gridView
				.findViewById(R.id.textViewTag);
		Item item = mTag.getmItems().get(position);
		tvTag.setText(item.getmText()[Constant.getLANGMODE_INPUT().value]);
		
		if (item == mSelectedTagItem)
			tvTag.setBackgroundResource(R.color.color_bg_tag_pressed);
		else
			tvTag.setBackgroundResource(R.color.color_bg_tag_normal);
		
//		ImageView ivIcon = (ImageView) gridView
//				.findViewById(R.id.imageViewIcon);

//		ivIcon.setImageResource(childCate.getmIdIcon());
		return gridView;
	}
 
	
	public int getCount() {
		return mTag.getmItems().size();
	}
 
	
	public Item getItem(int position) {
		return mTag.getmItems().get(position);
	}
 
	
	public long getItemId(int position) {
		return 0;
	}


	public Item getmSelectedTagItem() {
		return mSelectedTagItem;
	}


	public void setmSelectedTagItem(Item mSelectedTagItem) {
		this.mSelectedTagItem = mSelectedTagItem;
		notifyDataSetChanged();
	}

	
 
}
