package thesis.travelspeakbook.adapter;


import model.Category;
import thesis.travelspeakbook.R;
import thesis.travelspeakbook.current_data.Constant;
import android.R.color;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class SubCategoryIconSideBarAdapter extends BaseAdapter{
	private Context context;
	private Category mCategory;
	private Category mCategorySubSelected;
	
	public SubCategoryIconSideBarAdapter(Context context, Category category, Category categorySubSelected) {
		this.context = context;
		this.mCategory = category;
		mCategorySubSelected = categorySubSelected;
	}
	
	
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context
			.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
 
		View gridView;
 
		if (convertView == null) {
			gridView = new View(context);
			gridView = inflater.inflate(R.layout.one_sub_category_sidebar, null);
		} else {
			gridView = (View) convertView;
		}
		TextView tvLabel = (TextView) gridView.findViewById(R.id.textViewSubCate);
		Category childCate = ((Category)mCategory.get(position));
		tvLabel.setText(childCate.getText(Constant.getLANGMODE_INPUT().value));
		
		if (mCategory.getmItems().get(position) == mCategorySubSelected) {
			// style selected
			tvLabel.setTextColor(Color.BLACK);
			tvLabel.setBackgroundColor(Color.WHITE);
		}
		else {
			tvLabel.setTextColor(Color.WHITE);
			tvLabel.setBackgroundColor(Color.BLACK);
		}
		return gridView;
	}
 
	
	public int getCount() {
		return mCategory.size();
	}
 
	
	public Category getItem(int position) {
		return (Category)mCategory.get(position);
	}
 
	
	public long getItemId(int position) {
		return 0;
	}


	public Category getmCategorySubSelected() {
		return mCategorySubSelected;
	}


	public void setmCategorySubSelected(Category mCategorySubSelected) {
		this.mCategorySubSelected = mCategorySubSelected;
		//TODO: xem lai
		notifyDataSetChanged();
	}

	
 
}
