package thesis.travelspeakbook.custom_view;

import model.Item;
import model.TagSentence;
import thesis.travelspeakbook.current_data.Constant;
import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

public class TagOptionTextView extends TextView {
	public TagOptionTextView(Context context, TagSentence ts, int positionInSentence, Item selectedTagItem) {
		super(context);
		setData(ts, positionInSentence, selectedTagItem);
	}
	
	public TagOptionTextView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	public TagOptionTextView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}
	

	TagSentence mTag;
	int mPositionInSentence;
	Item mSelectedTagItem;
	
	public void setData(TagSentence ts, int positionInSentence, Item selectedTagItem) {
		mTag = ts;
		mPositionInSentence = positionInSentence;
		mSelectedTagItem = selectedTagItem;
		
		setText(mSelectedTagItem.getmText()[Constant.getLANGMODE_INPUT().value]);
	}
	
	/**
	 * @return the mTag
	 */
	public TagSentence getmTag() {
		return mTag;
	}

	/**
	 * @param mTag the mTag to set
	 */
	public void setmTag(TagSentence mTag) {
		this.mTag = mTag;
	}

	public int getmPositionInSentence() {
		return mPositionInSentence;
	}

	public void setmPositionInSentence(int mPositionInSentence) {
		this.mPositionInSentence = mPositionInSentence;
	}

	public Item getmSelectedTagItem() {
		return mSelectedTagItem;
	}

	public void setmSelectedTagItem(Item mSelectedTagItem) {
		this.mSelectedTagItem = mSelectedTagItem;
	}
	
}
