package thesis.travelspeakbook.custom_view;

import model.Item;
import model.TagSentence;
import thesis.travelspeakbook.current_data.Constant;
import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

public class TagChooseTextView extends TextView {
	public TagChooseTextView(Context context) {
		super(context);
	}
	
	public TagChooseTextView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	public TagChooseTextView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}
	
	TagSentence mTag;
	Item mTagSelectedItem;
	int mPositionInSentence;
	
	public void setData(TagSentence ts, Item tagSelectedItem, int positionInSentence) {
		mTag = ts;
		mTagSelectedItem = tagSelectedItem;
		mPositionInSentence = positionInSentence;
		
		String text = mTagSelectedItem.getmText()[Constant.getLANGMODE_INPUT().value];
		setText(text);
	}
	
	/**
	 * @return the mTag
	 */
	public TagSentence getmTag() {
		return mTag;
	}

	/**
	 * @param mTag the mTag to set
	 */
	public void setmTag(TagSentence mTag) {
		this.mTag = mTag;
	}

	public Item getmTagSelectedItem() {
		return mTagSelectedItem;
	}

	public void setmTagSelectedItem(Item mTagSelectedItem) {
		this.mTagSelectedItem = mTagSelectedItem;
	}

	public int getmPositionInSentence() {
		return mPositionInSentence;
	}

	public void setmPositionInSentence(int mPositionInSentence) {
		this.mPositionInSentence = mPositionInSentence;
	}
	
}
