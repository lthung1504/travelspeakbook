package thesis.travelspeakbook.custom_view;

import model.MySentence;
import thesis.code.ParseStatistic;
import thesis.travelspeakbook.R;
import thesis.travelspeakbook.current_data.Constant;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;

public class FavoriteIconImageButton extends ImageButton {
	static final int FAVORITE_ON_ICON_RESOURCE = R.drawable.favorite_on;
	static final int FAVORITE_OFF_ICON_RESOURCE = R.drawable.favorite_off;
	MySentence mSentence;
	public FavoriteIconImageButton(Context context) {
		super(context);
	}
	
	
	public FavoriteIconImageButton(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}


	public FavoriteIconImageButton(Context context, AttributeSet attrs) {
		super(context, attrs);
	}


	public FavoriteIconImageButton(Context context, MySentence sentence) {
		super(context);
		mSentence = sentence;
		
		setUpUI();
		setUpFunction();
	}


	private void setUpFunction() {
		setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				if (mSentence.ismIsFavorite()) {
					setOffImage();
				}
				else {
					setOnImage();
				}
			}

			
		});
	}
	
	void setOffImage() {
		mSentence.setmIsFavorite(false);
		setImageResource(FAVORITE_OFF_ICON_RESOURCE);
		Constant.CATE_FAVORITE.getmItems().remove(mSentence);
		Toast.makeText(ParseStatistic.getmContext(), "Removed sentence from favorite context", Toast.LENGTH_SHORT).show();
	}
	
	private void setOnImage() {
		mSentence.setmIsFavorite(true);
		setImageResource(FAVORITE_ON_ICON_RESOURCE);
		// add to cate favo
		Constant.CATE_FAVORITE.add(mSentence);
		Toast.makeText(ParseStatistic.getmContext(), "Added sentence to favorite context", Toast.LENGTH_SHORT).show();

	}


	private void setUpUI() {
		setBackgroundResource(android.R.color.transparent);
		if (mSentence.ismIsFavorite()) {
			setImageResource(FAVORITE_ON_ICON_RESOURCE);
		}
		else {
			setImageResource(FAVORITE_OFF_ICON_RESOURCE);
		}
	}


	/**
	 * @return the mSentence
	 */
	public MySentence getmSentence() {
		return mSentence;
	}


	/**
	 * @param mSentence the mSentence to set
	 */
	public void setmSentence(MySentence mSentence) {
		this.mSentence = mSentence;
		
		setUpUI();
		
		setUpFunction();
	}
	

}
