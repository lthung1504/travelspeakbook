package thesis.travelspeakbook;

import java.util.List;

import model.Category;
import model.MySentence;
import thesis.travelspeakbook.current_data.Constant;
import thesis.travelspeakbook.fragment.SearchBarFragment;
import thesis.travelspeakbook.fragment.SearchBarFragment.OnSearchFinishedListener;
import thesis.travelspeakbook.fragment.SentencesFragment;
import thesis.travelspeakbook.fragment.SentencesFragment.OnSentenceSelectedListener;
import thesis.travelspeakbook.fragment.SideBarCateFragment;
import thesis.travelspeakbook.fragment.SideBarCateFragment.OnCategorySideBarSelectedListener;
import thesis.travelspeakbook.fragment.SideBarSubCateFragment;
import thesis.travelspeakbook.fragment.SideBarSubCateFragment.OnSubCategorySelectedListener;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;
public class SearchActivity extends FragmentActivity implements OnSearchFinishedListener, OnSentenceSelectedListener, OnSubCategorySelectedListener, OnCategorySideBarSelectedListener {
	
	private static final int WIDTH_SLIDEMENU = 100;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		overridePendingTransition(R.anim.transition_right_to_left,
				R.anim.transition_middle_to_left);
//		setContentView(R.layout.page_search);
		setTitle("Search");
		setContentView(R.layout.page_search);
		setUpVar();
		setUpUI();
		
		
		// TODO: dummy
//		mSubCateSidebarFragment = new SideBarSubCateFragment(Constant.CURRENT_CATE);
//		mSearchBarFragment = new SearchBarFragment(Constant.CURRENT_CATE, "");
//		mSentenceFragment = new SentencesFragment();
//		// show sliding menu cate
		
		
		
		setUpSideBar();
		
//		setBehindContentView(R.layout.menu_frame);
//		getSlidingMenu().setSlidingEnabled(true);
//		getSlidingMenu().setTouchModeAbove(SlidingMenu.TOUCHMODE_FULLSCREEN);
//		// show home as up so we can toggle
//		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//
//		// set the Above View Fragment
////		if (savedInstanceState != null)
////			mContent = getSupportFragmentManager().getFragment(savedInstanceState, "mContent");
////		if (mContent == null)
////			mContent = new BirdGridFragment(0);	
//		getSupportFragmentManager()
//		.beginTransaction()
//		.replace(R.id.fragment_container, mSentenceFragment) // show sentence fragment
//		.commit();
//
//		// set the Behind View Fragment
//		getSupportFragmentManager()
//		.beginTransaction()
//		.replace(R.id.menu_frame, mSubCateSidebarFragment)
//		.commit();
//		// customize the SlidingMenu
//		SlidingMenu sm = getSlidingMenu();
////		sm.setBehindOffsetRes(R.dimen.slidingmenu_offset);
////		sm.setShadowWidthRes(R.dimen.shadow_width);
////		sm.setShadowDrawable(R.drawable.shadow);
//		sm.setBehindScrollScale(0.25f);
//		sm.setFadeDegree(0.25f);
		
		
	}
	private void setUpVar() {
		// TODO Auto-generated method stub
		mMenuSidebar = new SlidingMenu(this);
	}
	SlidingMenu mMenuSidebar;
	private void setUpSideBar() {
		mMenuSidebar.setMode(SlidingMenu.RIGHT);
		mMenuSidebar.setTouchModeAbove(SlidingMenu.TOUCHMODE_FULLSCREEN);
//		menu.setShadowWidthRes(R.dimen.shadow_width);
//		menu.setShadowDrawable(R.drawable.shadow);
//		menu.setBehindOffsetRes(R.dimen.slidingmenu_offset);
		mMenuSidebar.setBehindWidth(WIDTH_SLIDEMENU);
//		menu.setAboveOffsetRes(R.dimen.slidingmenu_offset);
		mMenuSidebar.setFadeDegree(0.35f);
		mMenuSidebar.attachToActivity(this, SlidingMenu.SLIDING_CONTENT);
		
		mMenuSidebar.setMenu(R.layout.menu_right);
		getSupportFragmentManager()
		.beginTransaction()
		.replace(R.id.fragment_menu_content_right, new SideBarCateFragment())
		.commit();
		
		
	}
	SentencesFragment mSentenceFragment;
	SearchBarFragment mSearchBarFragment;
	SideBarSubCateFragment mSubCateSidebarFragment;
	private void setUpUI() {
		setUpUIStatic();
		
		mSearchBarFragment = new SearchBarFragment(Constant.CURRENT_CATE, "");
		Constant.showFragment(getSupportFragmentManager(), mSearchBarFragment, R.id.fragment_containerSearch, false);
		
		mSentenceFragment = new SentencesFragment();
		Constant.showFragment(getSupportFragmentManager(), mSentenceFragment, R.id.fragment_container, false);
		
		
//		if (hasSubCate(Constant.CURRENT_CATE)) {
//			mSubCateSidebarFragment = new SideBarSubCateFragment(Constant.CURRENT_CATE);
//			Constant.showFragment(getSupportFragmentManager(), mSubCateSidebarFragment , R.id.fragment_sidebar_sub_cate, false);
//		}		
	}
	private void setUpUIStatic() {
		// update UI xung quanh
		TextView tv = (TextView) findViewById(R.id.textViewCurrentContext);
		
		ImageView iv = (ImageView) findViewById(R.id.imageViewCurrentContext);
		int idResImageCate;
		if (Constant.CURRENT_CATE == Constant.CURRENT_ROOT_CATE) {
			idResImageCate = R.drawable.search_icon;
			tv.setText(Constant.RES_SEARCH[Constant.getLANGMODE_INPUT().value]);
		}
		else {
			// error
			idResImageCate = Constant.RES_ID_MIDDLE_ICON_CATE.get(Constant.CURRENT_CATE);
			tv.setText(Constant.CURRENT_CATE.getmText()[Constant.getLANGMODE_INPUT().value]);
		}
		iv.setImageResource(idResImageCate);
		
		if (hasSubCate(Constant.CURRENT_CATE) && Constant.CURRENT_CATE != Constant.CURRENT_ROOT_CATE) {
			findViewById(R.id.fragment_sidebar_subcate).setVisibility(View.VISIBLE);
			mSubCateSidebarFragment = new SideBarSubCateFragment(Constant.CURRENT_CATE);
			Constant.showFragment(getSupportFragmentManager(), mSubCateSidebarFragment, R.id.fragment_sidebar_subcate, false);
		}
		else {
			// hide this fragment
			findViewById(R.id.fragment_sidebar_subcate).setVisibility(View.GONE);
		}
	}
	boolean hasSubCate(Category c) {
		if (c.getmItems().size()==0)
			return false;
		if (c.get(0) instanceof Category)
			return true;
		return false;
	}

	public void onSearchFinished(List<MySentence> sentencesFound,
			String textSearch) {
		mSentenceFragment.updateData(sentencesFound, textSearch);
	}

	public void onSentenceSelected(MySentence sentenceSelected) {
		// move to edit page
		Intent i = new Intent(SearchActivity.this, EditActivity.class);
		Constant.CURRENT_SENTENCE = sentenceSelected;
		startActivity(i);
	}


	public void onSubCategorySelected(Category cateSelected) {
		// update cate
		Constant.CURRENT_CATE = cateSelected;
		// update search bar & take basic sentence
		mSearchBarFragment.updateCate(cateSelected);
		
	}


	public void onCategorySideBarSelected(Category cateSelected) {
		// TODO Auto-generated method stub
		Constant.CURRENT_CATE = cateSelected;
		mSearchBarFragment.updateCate(cateSelected);
		// update UI context
		setUpUIStatic();
		// update subcate
		// restart activity TODO: sau nay chinh lai cho no update dc
//		Intent i = getIntent();
//		finish();
//		overridePendingTransition(0, 0);
//		startActivity(i);
//		if (!hasSubCate(cateSelected)) {
//			// disable subcate
//			mMenuSidebar.
//		}
	}
	public void onHomeSideBarSelected() {
		// go to main activity
		onBackPressed();
	}

}
