package thesis.travelspeakbook;

import model.Category;
import thesis.code.ParseStatistic;
import thesis.travelspeakbook.current_data.Constant;
import thesis.travelspeakbook.fragment.CategoriesFragment;
import thesis.travelspeakbook.fragment.CategoriesFragment.OnCategorySelectedListener;
import thesis.travelspeakbook.fragment.CategoriesFragment.OnCreatedCatesListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MainActivity extends FragmentActivity implements OnCategorySelectedListener, OnCreatedCatesListener{
	private static final String EMPTY_STRING = "";

	private static final String TAG = "MainActivity";
	
	SharedPreferences.Editor mEditor;
	SharedPreferences mSharePreference;
	
	// TODO: Improve later, temp cate
//	SearchBarFragment mSearchBarFragment;
	
	
	public void onCreate(Bundle savedInstanceState) {
		Log.d(TAG, "onCreate mainactivity");
		super.onCreate(savedInstanceState);
		overridePendingTransition(R.anim.transition_right_to_left,
				R.anim.transition_middle_to_left);
		Constant.CURRENT_ACTIVITY = this;

		setUpVar();
		setUpUI();
	}
	
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}
	
	protected void onDestroy() {
		super.onDestroy();
		ParseStatistic.save_database_from_android(Constant.DATABASE);
	}
	
	void setUpVar() {
//		mSearchBarFragment = new SearchBarFragment(Constant.CURRENT_ROOT_CATE, EMPTY_STRING); 
		setupStorage();
	}
	private void setUpUI() {
		Log.d(TAG, "setUpUI main activity");
		
		Constant.CURRENT_CATE = Constant.CURRENT_ROOT_CATE;
		setContentView(R.layout.activity_main);

//		showFragmentSearchBar(mSearchBarFragment);
		showFragmentMainFrame(new CategoriesFragment(Constant.CURRENT_ROOT_CATE), false);
		
//		ImageButton ibInput = (ImageButton) findViewById(R.id.imageButtonInputLang);
//		ibInput.setImageResource(Constant.RES_FLAG_SMALL_ON[Constant.getLANGMODE_INPUT().value]);
//		ibInput.setOnClickListener(new OnClickListener() {
//			
//			public void onClick(View v) {
//				// choose lang activity
//				Constant.isInputLang = true;
//				Intent i = new Intent(MainActivity.this, ChooseLanguageActivity.class);
//				startActivity(i);
//			}
//		});
//		ImageButton ibOutput = (ImageButton) findViewById(R.id.imageButtonOutputLang);
//		ibOutput.setImageResource(Constant.RES_FLAG_SMALL_ON[Constant.getLANGMODE_OUTPUT().value]);
//		ibOutput.setOnClickListener(new OnClickListener() {
//			
//			
//			public void onClick(View v) {
//				// choose lang activity
//				Constant.isInputLang = false;
//				Intent i = new Intent(MainActivity.this, ChooseLanguageActivity.class);
//				startActivity(i);
//			}
//		});
		
		// set function for change language icon
		setUpTopBarLangIcon();
		Button btn = (Button) findViewById(R.id.buttonSearch);
		btn.setText(Constant.RES_TYPE_SEARCH[Constant.getLANGMODE_INPUT().value]);
		btn.setClickable(false);
		LinearLayout llSearchButton = (LinearLayout) findViewById(R.id.linearLayoutSearchButton);
		llSearchButton.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Constant.CURRENT_CATE = Constant.CURRENT_ROOT_CATE;
				Intent i = new Intent(MainActivity.this, SearchActivity.class);
				startActivity(i);
			}
		});
		
		((TextView) findViewById(R.id.textViewCommand)).setText(Constant.RES_COMMAND[Constant.getLANGMODE_INPUT().value]);
	}
	static int[] resIDFlag = {R.drawable.eng_flag, 
		R.drawable.viet_flag, R.drawable.lao_flag, R.drawable.cam_flag};
	void setUpTopBarLangIcon() {
		ImageButton ib = (ImageButton) findViewById(R.id.imageButtonInputLang);
		ib.setImageResource(resIDFlag[Constant.getLANGMODE_INPUT().value]);
		ib.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				// go to page choose language activity
				Intent i = new Intent(MainActivity.this, ChooseLanguageActivity.class);
				startActivity(i);
			}
		});
		ib = (ImageButton) findViewById(R.id.imageButtonOutputLang);
		ib.setImageResource(resIDFlag[Constant.getLANGMODE_OUTPUT().value]);
		ib.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				// go to page choose language activity
				Intent i = new Intent(MainActivity.this, ChooseLanguageActivity.class);
				startActivity(i);
			}
		});
	}
	void setupStorage() {
		mSharePreference = getSharedPreferences(Constant.TRAVEL_STORAGE, 0);
		mEditor = mSharePreference.edit();
	}
	
	public void onCategorySelected(Category cateSelected) {
		// move to search activity for this cate
		Constant.CURRENT_CATE = cateSelected;
//		mSearchBarFragment.updateCate(Constant.CURRENT_CATE);
		Intent i = new Intent(MainActivity.this, SearchActivity.class);
		startActivity(i);
		
//		int size = cateSelected.size();
//		if (size == 0) {
//			showFragmentMainFrame(new SentencesFragment((List<MySentence>)cateSelected.accept(new CategorySearchVisitor("")), EMPTY_STRING), true);
//			return;
//		}
//		// size != 0
//		Item item = cateSelected.get(0);
//		if (item instanceof MySentence) {
//			showFragmentMainFrame(new SentencesFragment((List<MySentence>)cateSelected.accept(new CategorySearchVisitor("")), EMPTY_STRING), true);
//		}
//		else if (item instanceof Category) {
//			// show fragment contexts
//			showFragmentMainFrame(new CategoriesFragment(cateSelected), true);
//		}
	}
	private void showFragmentMainFrame(Fragment f, boolean isKeepHistory) {
		Constant.showFragment(getSupportFragmentManager(), f, R.id.fragment_container, isKeepHistory);
	}
	
//	private void showFragmentSearchBar(Fragment f) {
//		Constant.showFragment(getSupportFragmentManager(), f, R.id.fragment_containerSearch, false);
//	}
	
	
//	public void onSentenceSelected(MySentence sentenceSelected) {
////		showFragmentMainFrame(new EditSentenceFragment(sentenceSelected), true);
//		// move to edit activity
//		Intent i = new Intent(MainActivity.this, EditActivity.class);
//		Constant.CURRENT_SENTENCE = sentenceSelected;
//		startActivity(i);
//	}
//	
	int countSearch = 0;
	
//	public void onSearchFinished(List<MySentence> sentencesFound, String textSearch) {
//		// show these sentences
//		Fragment f = getSupportFragmentManager().findFragmentById(R.id.fragment_container);
//		if (f instanceof SentencesFragment) {
//			// update this fragment
//			((SentencesFragment) f).updateData(sentencesFound, textSearch);
//		}
//		else { // replace new SentencesFragment
//			showFragmentMainFrame(new SentencesFragment(sentencesFound, textSearch), true);
//		}
//	}
//	
	public void onCreatedCates(Category categoryContainSubCate) {
		// update search cate fragment
//		mSearchBarFragment.updateCate(categoryContainSubCate);
	}

	
}
