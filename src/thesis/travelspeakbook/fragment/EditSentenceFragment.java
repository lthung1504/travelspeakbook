package thesis.travelspeakbook.fragment;

import thesis.travelspeakbook.R;
import thesis.travelspeakbook.current_data.Constant;
import model.MySentence;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class EditSentenceFragment extends Fragment {
	MySentence mSentence;
	
	public EditSentenceFragment(MySentence sentence) {
		mSentence = sentence;
	}
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater
				.inflate(R.layout.page_translate, container, false);
		
		TextView tv = (TextView)view.findViewById(R.id.textViewTranslate);
		tv.setText(mSentence.getmText()[Constant.getLANGMODE_OUTPUT().value]);
		
		tv = (TextView)view.findViewById(R.id.textViewOriginal);
		tv.setText(mSentence.getmText()[Constant.getLANGMODE_INPUT().value]);
		
		
		
		
		return view;
	}
}
