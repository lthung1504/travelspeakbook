package thesis.travelspeakbook.fragment;

import model.Category;
import thesis.travelspeakbook.MainActivity;
import thesis.travelspeakbook.R;
import thesis.travelspeakbook.SearchActivity;
import thesis.travelspeakbook.adapter.CategoryIconSideBarAdapter;
import thesis.travelspeakbook.current_data.Constant;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;

public class SideBarCateFragment extends Fragment{
	OnCategorySideBarSelectedListener mCallback;

    // The container Activity must implement this interface so the frag can deliver messages
    public interface OnCategorySideBarSelectedListener {
        public void onCategorySideBarSelected(Category cateSelected);
        public void onHomeSideBarSelected();
    }

	public SideBarCateFragment() {
	}
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_sidebar_cates, container, false);

		// set grid view
		ListView lvListCate = (ListView) view
				.findViewById(R.id.listViewSideBarCates);
		ImageView ivHome = (ImageView) view.findViewById(R.id.imageViewHome);
		ivHome.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				mCallback.onHomeSideBarSelected();
			}
		});
		
		ImageView ivFavorite = (ImageView) view.findViewById(R.id.imageViewFavorite);
		ivFavorite.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				mCallback.onCategorySideBarSelected(Constant.CATE_FAVORITE);
			}
		});
		
		
		lvListCate.setAdapter(new CategoryIconSideBarAdapter(getActivity(), Constant.CURRENT_ROOT_CATE));

		lvListCate.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View v,
					int position, long id) {
				// check if is context then show context
				// else then show fragment sentences
				Category cateClicked = (Category) Constant.CURRENT_ROOT_CATE.get(position);
				mCallback.onCategorySideBarSelected(cateClicked);
			}
		});
		return view;
	}
	@Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception.
        try {
            mCallback = (OnCategorySideBarSelectedListener) activity;
            
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnCategorySideBarSelectedListener");
        }
    }

}
