package thesis.travelspeakbook.fragment;

import thesis.travelspeakbook.R;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class EmptyFragment extends Fragment {
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.empty_cate, container);
		return view;
	}
	public EmptyFragment() {
		super();
	}
	
}
