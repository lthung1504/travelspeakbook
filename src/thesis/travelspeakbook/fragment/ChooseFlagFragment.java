package thesis.travelspeakbook.fragment;

import model.Item;
import thesis.travelspeakbook.R;
import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageButton;

public class ChooseFlagFragment extends Fragment {
	// 1->4 cho tung lang
	int mIdSelectLang;
	
	static int[] resID = {R.id.imageButtonEng,
		R.id.imageButtonViet,
		R.id.imageButtonLao, 
		R.id.imageButtonCam	};
	static int[] resID_image_on = {R.drawable.eng_on,
		R.drawable.viet_on,
		R.drawable.lao_on,
		R.drawable.cam_on,
		};
	static int[] resID_image_off = {R.drawable.eng_off,
		R.drawable.viet_off,
		R.drawable.lao_off,
		R.drawable.cam_off,
		};
	
	OnFlagSelectedListener mCallback;

    // The container Activity must implement this interface so the frag can deliver messages
    public interface OnFlagSelectedListener {
        public void onFlagSelected(int idFlagSelected, int idFrag);
    }

	
	public ChooseFlagFragment(int idSelectLang) {
		mIdSelectLang = idSelectLang;
	}
	
	public ChooseFlagFragment() {
		mIdSelectLang = -1;
	}
	
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		final View view = inflater
				.inflate(R.layout.fragment_choose_flag, container, false);
		// check choose flag => disable flag
		if (mIdSelectLang >= 0) {
			ImageButton ib = (ImageButton) view.findViewById(resID[mIdSelectLang]);
			ib.setImageResource(resID_image_off[mIdSelectLang]);
			ib.setEnabled(false);
		}
		for (int i = 0; i<Item.TOTAL_LANGS; i++) {
			ImageButton ib = (ImageButton) view.findViewById(resID[i]);
			ib.setTag(Integer.valueOf(i));
			ib.setOnClickListener(new OnClickListener() {
				
				public void onClick(View v) {
					// click this button then disable another image
					// event chosen lang
					ImageButton ib = (ImageButton) v;
					int idClicked = (Integer)ib.getTag();
					ib.setImageResource(resID_image_on[idClicked]);
					for (int i = 0; i<Item.TOTAL_LANGS; i++) {
						if (i != idClicked) {
							ImageButton tempIb = (ImageButton) view.findViewById(resID[i]);
							tempIb.setImageResource(resID_image_off[i]);
						}
					}
					mCallback.onFlagSelected(idClicked, getId());
				}
			});
		}
		
		return view;
		
		
	}
	@Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception.
        try {
            mCallback = (OnFlagSelectedListener) activity;
            
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }

	public int getmIdSelectLang() {
		return mIdSelectLang;
	}

	public void setmIdSelectLang(int mIdSelectLang) {
		this.mIdSelectLang = mIdSelectLang;
	}
}
