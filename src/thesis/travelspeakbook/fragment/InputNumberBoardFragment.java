package thesis.travelspeakbook.fragment;

import model.TagSentence;
import thesis.travelspeakbook.R;
import thesis.travelspeakbook.current_data.Constant;
import thesis.travelspeakbook.custom_view.TagOptionTextView;
import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

public class InputNumberBoardFragment extends Fragment {
	// The container Activity must implement this interface so the frag can deliver messages
    public interface OnKeyNumberSelectedListener {
        public void onKeyNumberSelected(String textInputed, TagOptionTextView totvModify);
    }
	OnKeyNumberSelectedListener mCallback;

    String mTextTyped = "";
    TagOptionTextView mTextViewModify;
    
    public InputNumberBoardFragment(TagOptionTextView textViewModify) {
    	mTextViewModify = textViewModify;
    }
	
	private void addNumToText(String added) {
		mTextTyped += added;
		mCallback.onKeyNumberSelected(this.mTextTyped, mTextViewModify);
	}
	
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception.
        try {
            mCallback = (OnKeyNumberSelectedListener) activity;
            
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }
	
	
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater
				.inflate(R.layout.fragment_input_number_board, container, false);
		setUpBtnNumKey(view);
		
		return view;
		
	}
	void setUpBtnNumKey(View view) {
		mTextTyped = "";
		Button btn0;
		btn0 = (Button) view.findViewById(R.id.button0);
		btn0.setOnClickListener(new OnClickListener() {
			
			
			public void onClick(View v) {
				addNumToText("0");
			}
		});
		btn0 = (Button) view.findViewById(R.id.button1);
		btn0.setOnClickListener(new OnClickListener() {
			
			
			public void onClick(View v) {
				addNumToText("1");
			}
		});
		btn0 = (Button) view.findViewById(R.id.button2);
		btn0.setOnClickListener(new OnClickListener() {
			
			
			public void onClick(View v) {
				addNumToText("2");
			}
		});
		btn0 = (Button) view.findViewById(R.id.button3);
		btn0.setOnClickListener(new OnClickListener() {
			
			
			public void onClick(View v) {
				addNumToText("3");
			}
		});
		btn0 = (Button) view.findViewById(R.id.button4);
		btn0.setOnClickListener(new OnClickListener() {
			
			
			public void onClick(View v) {
				addNumToText("4");
			}
		});
		btn0 = (Button) view.findViewById(R.id.button5);
		btn0.setOnClickListener(new OnClickListener() {
			
			
			public void onClick(View v) {
				addNumToText("5");
			}
		});
		btn0 = (Button) view.findViewById(R.id.button6);
		btn0.setOnClickListener(new OnClickListener() {
			
			
			public void onClick(View v) {
				addNumToText("6");
			}
		});
		btn0 = (Button) view.findViewById(R.id.button7);
		btn0.setOnClickListener(new OnClickListener() {
			
			
			public void onClick(View v) {
				addNumToText("7");
			}
		});
		btn0 = (Button) view.findViewById(R.id.button8);
		btn0.setOnClickListener(new OnClickListener() {
			
			
			public void onClick(View v) {
				addNumToText("8");
			}
		});
		btn0 = (Button) view.findViewById(R.id.button9);
		btn0.setOnClickListener(new OnClickListener() {
			
			
			public void onClick(View v) {
				addNumToText("9");
			}
		});
		btn0 = (Button) view.findViewById(R.id.buttonClear);
		btn0.setText(Constant.RES_CLEAR[Constant.getLANGMODE_INPUT().value]);
		btn0.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				setmTextTyped("");
				mCallback.onKeyNumberSelected(mTextTyped, mTextViewModify);
			}
		});
		btn0 = (Button) view.findViewById(R.id.buttonDot);
		btn0.setText(".");
		btn0.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				addNumToText(".");
			}
		});
		
	}
	/**
	 * @return the mTextTyped
	 */
	public String getmTextTyped() {
		return mTextTyped;
	}
	/**
	 * @param mTextTyped the mTextTyped to set
	 */
	public void setmTextTyped(String mTextTyped) {
		this.mTextTyped = mTextTyped;
		
		
	}
	
}
