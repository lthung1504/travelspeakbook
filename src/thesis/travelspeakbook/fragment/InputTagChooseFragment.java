package thesis.travelspeakbook.fragment;

import thesis.travelspeakbook.R;
import thesis.travelspeakbook.adapter.TagItemAdapter;
import thesis.travelspeakbook.custom_view.TagChooseTextView;
import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;

public class InputTagChooseFragment extends Fragment{
	TagChooseTextView mTagChooseTextView;
	OnTagSelectedListener mCallback;

    // The container Activity must implement this interface so the frag can deliver messages
    public interface OnTagSelectedListener {
        public void onTagItemSelected(TagChooseTextView tagChooseTextView, int position);
    }

    public InputTagChooseFragment(TagChooseTextView tagChooseTextView) {
		mTagChooseTextView = tagChooseTextView;
	}
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater
				.inflate(R.layout.fragment_tags, container, false);
		// set grid view
		GridView gvTags = (GridView) view
				.findViewById(R.id.gridViewTags);
		final TagItemAdapter tia = new TagItemAdapter(getActivity(), mTagChooseTextView.getmTag(), mTagChooseTextView.getmTagSelectedItem());
		gvTags.setAdapter(tia);

		gvTags.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View v,
					int position, long id) {
				tia.setmSelectedTagItem(mTagChooseTextView.getmTag().getmItems().get(position));
				mCallback.onTagItemSelected(mTagChooseTextView, position);
			}
		});
		return view;
	}
	@Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception.
        try {
            mCallback = (OnTagSelectedListener) activity;
            
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnTagSelectedListener");
        }
    }
}
