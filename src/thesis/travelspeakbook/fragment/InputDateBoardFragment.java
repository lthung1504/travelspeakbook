package thesis.travelspeakbook.fragment;


import thesis.travelspeakbook.R;
import thesis.travelspeakbook.current_data.Constant;
import thesis.travelspeakbook.custom_view.TagOptionTextView;
import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;

public class InputDateBoardFragment extends Fragment {
	// The container Activity must implement this interface so the frag can deliver messages
    public interface OnKeyDateSelectedListener {
        public void onKeyDateSelected(String textInputed, String textTranslate, TagOptionTextView totvModify);
    }
	OnKeyDateSelectedListener mCallback;

    String mTextTyped = "";
    TagOptionTextView mTextViewModifyDate;
	boolean mIsUseDateKey = false; 
    
    public InputDateBoardFragment(TagOptionTextView totvModify) {
    	mTextViewModifyDate = totvModify;
    }
	@Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception.
        try {
            mCallback = (OnKeyDateSelectedListener) activity;
            
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater
				.inflate(R.layout.fragment_input_date_board, container, false);
//		DatePicker datePicked = (DatePicker) view.findViewById(R.id.datePickerDate);
		setUpBtnDateKey(view);
		return view;
		
	}
	private void addNumToText(String added) {
		if (!mIsUseDateKey) {
			mTextTyped += added;
		}
		else {
			mTextTyped = added;
			mIsUseDateKey = false;
		}
		mCallback.onKeyDateSelected(this.mTextTyped, mTextTyped, mTextViewModifyDate);
	}
	Button btn0;
	void setUpBtnNumKey(View view) {
		mTextTyped = "";
		btn0 = (Button) view.findViewById(R.id.button0);
		btn0.setOnClickListener(new OnClickListener() {
			
			
			public void onClick(View v) {
				addNumToText("0");
			}
		});
		btn0 = (Button) view.findViewById(R.id.button1);
		btn0.setOnClickListener(new OnClickListener() {
			
			
			public void onClick(View v) {
				addNumToText("1");
			}
		});
		btn0 = (Button) view.findViewById(R.id.button2);
		btn0.setOnClickListener(new OnClickListener() {
			
			
			public void onClick(View v) {
				addNumToText("2");
			}
		});
		btn0 = (Button) view.findViewById(R.id.button3);
		btn0.setOnClickListener(new OnClickListener() {
			
			
			public void onClick(View v) {
				addNumToText("3");
			}
		});
		btn0 = (Button) view.findViewById(R.id.button4);
		btn0.setOnClickListener(new OnClickListener() {
			
			
			public void onClick(View v) {
				addNumToText("4");
			}
		});
		btn0 = (Button) view.findViewById(R.id.button5);
		btn0.setOnClickListener(new OnClickListener() {
			
			
			public void onClick(View v) {
				addNumToText("5");
			}
		});
		btn0 = (Button) view.findViewById(R.id.button6);
		btn0.setOnClickListener(new OnClickListener() {
			
			
			public void onClick(View v) {
				addNumToText("6");
			}
		});
		btn0 = (Button) view.findViewById(R.id.button7);
		btn0.setOnClickListener(new OnClickListener() {
			
			
			public void onClick(View v) {
				addNumToText("7");
			}
		});
		btn0 = (Button) view.findViewById(R.id.button8);
		btn0.setOnClickListener(new OnClickListener() {
			
			
			public void onClick(View v) {
				addNumToText("8");
			}
		});
		btn0 = (Button) view.findViewById(R.id.button9);
		btn0.setOnClickListener(new OnClickListener() {
			
			
			public void onClick(View v) {
				addNumToText("9");
			}
		});
		btn0 = (Button) view.findViewById(R.id.buttonClear);
		btn0.setText(Constant.RES_CLEAR[Constant.getLANGMODE_INPUT().value]);
		btn0.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				setmTextTyped("");
				mCallback.onKeyDateSelected(mTextTyped, mTextTyped, mTextViewModifyDate);
			}
		});
	}
	
	void setUpBtnDateKey(View view) {
		setUpBtnNumKey(view);
		btn0 = (Button) view.findViewById(R.id.buttonToday);
		btn0.setText(Constant.RES_TODAY[Constant.getLANGMODE_INPUT().value]);
		btn0.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				mIsUseDateKey = true;
				mTextTyped = Constant.RES_TODAY[Constant.getLANGMODE_OUTPUT().value];
				String textOriginal = Constant.RES_TODAY[Constant.getLANGMODE_INPUT().value];
 
				mCallback.onKeyDateSelected(mTextTyped, textOriginal, mTextViewModifyDate);
			}
		});
		btn0 = (Button) view.findViewById(R.id.buttonYesterday);
		btn0.setText(Constant.RES_YESTERDAY[Constant.getLANGMODE_INPUT().value]);
		btn0.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				mIsUseDateKey = true;
				mTextTyped = Constant.RES_YESTERDAY[Constant.getLANGMODE_OUTPUT().value];
				String textOriginal = Constant.RES_YESTERDAY[Constant.getLANGMODE_INPUT().value];
				
				mCallback.onKeyDateSelected(mTextTyped, textOriginal, mTextViewModifyDate);
			}
		});
		
		btn0 = (Button) view.findViewById(R.id.buttonTomorrow);
		btn0.setText(Constant.RES_TOMORROW[Constant.getLANGMODE_INPUT().value]);
		btn0.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				mIsUseDateKey = true;
				mTextTyped = Constant.RES_TOMORROW[Constant.getLANGMODE_OUTPUT().value];
				String textOriginal = Constant.RES_TOMORROW[Constant.getLANGMODE_INPUT().value];
				 
				mCallback.onKeyDateSelected(mTextTyped, textOriginal, mTextViewModifyDate);
			}
		});
		btn0 = (Button) view.findViewById(R.id.buttonSplash);
		btn0.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				addNumToText("/");
			}
		});
		btn0 = (Button) view.findViewById(R.id.buttonAM);
		btn0.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				addNumToText(" AM ");
			}
		});
		btn0 = (Button) view.findViewById(R.id.buttonPM);
		btn0.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				addNumToText(" PM ");
			}
		});
		btn0 = (Button) view.findViewById(R.id.buttonColon);
		btn0.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				addNumToText(":");
			}
		});
		
	}
	/**
	 * @return the mTextTyped
	 */
	public String getmTextTyped() {
		return mTextTyped;
	}
	/**
	 * @param mTextTyped the mTextTyped to set
	 */
	public void setmTextTyped(String mTextTyped) {
		this.mTextTyped = mTextTyped;
		mCallback.onKeyDateSelected(this.mTextTyped, mTextTyped, mTextViewModifyDate);
	}
	
}
