package thesis.travelspeakbook.fragment;

import model.Category;
import model.Item;
import thesis.travelspeakbook.R;
import thesis.travelspeakbook.adapter.CategoryIconAdapter;
import thesis.travelspeakbook.current_data.Constant;
import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;

public class CategoriesFragment extends Fragment {
	Category mCategoryContainSubCate;
	OnCategorySelectedListener mCallback;
	OnCreatedCatesListener mCallbackCreatedCates;

    // The container Activity must implement this interface so the frag can deliver messages
    public interface OnCategorySelectedListener {
        public void onCategorySelected(Category cateSelected);
    }
    public interface OnCreatedCatesListener {
    	public void onCreatedCates(Category categoryContainSubCate);
    }

	public CategoriesFragment(Category categoryContainSubCate) {
		mCategoryContainSubCate = categoryContainSubCate;
	}
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_contexts, container, false);

		// set grid view
		GridView gvCates = (GridView) view
				.findViewById(R.id.gridViewCategories);
		// create list sub cate
		final Category showCate = new Category("temp", Constant.CURRENT_ROOT_CATE);
		showCate.add(Constant.CATE_FAVORITE);
		showCate.getmItems().addAll(Constant.CURRENT_ROOT_CATE.getmItems());

		gvCates.setAdapter(new CategoryIconAdapter(getActivity(), showCate));

		gvCates.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View v,
					int position, long id) {
				// check if is context then show context
				// else then show fragment sentences
				Category cateClicked = (Category) showCate.getmItems().get(position);
				mCallback.onCategorySelected(cateClicked);
			}
		});
		mCallbackCreatedCates.onCreatedCates(mCategoryContainSubCate);
		return view;
	}
		
	@Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception.
        try {
            mCallback = (OnCategorySelectedListener) activity;
            mCallbackCreatedCates = (OnCreatedCatesListener) activity;
            
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }
	/**
	 * @return the mCategoryContainSubCate
	 */
	public Category getmCategoryContainSubCate() {
		return mCategoryContainSubCate;
	}
	/**
	 * @param mCategoryContainSubCate the mCategoryContainSubCate to set
	 */
	public void setmCategoryContainSubCate(Category mCategoryContainSubCate) {
		this.mCategoryContainSubCate = mCategoryContainSubCate;
	}

}
