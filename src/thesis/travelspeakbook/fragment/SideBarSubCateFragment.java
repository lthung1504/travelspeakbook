package thesis.travelspeakbook.fragment;

import model.Category;
import thesis.travelspeakbook.R;
import thesis.travelspeakbook.adapter.CategoryIconAdapter;
import thesis.travelspeakbook.adapter.SubCategoryIconSideBarAdapter;
import thesis.travelspeakbook.fragment.CategoriesFragment.OnCategorySelectedListener;
import thesis.travelspeakbook.fragment.CategoriesFragment.OnCreatedCatesListener;
import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;

public class SideBarSubCateFragment extends Fragment{
	Category mCategoryContainSubCate;
	OnSubCategorySelectedListener mCallback;
	Category mCategorySubSelected;

    // The container Activity must implement this interface so the frag can deliver messages
    public interface OnSubCategorySelectedListener {
        public void onSubCategorySelected(Category cateSelected);
    }

	public SideBarSubCateFragment(Category categoryContainSubCate) {
		mCategoryContainSubCate = categoryContainSubCate;
	}
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_sidebar_sub_cates, container, false);
		
		// set grid view
		ListView lvListCate = (ListView) view
				.findViewById(R.id.listViewSubCates);
		final SubCategoryIconSideBarAdapter scisa = new SubCategoryIconSideBarAdapter(getActivity(), mCategoryContainSubCate, mCategorySubSelected);
		lvListCate.setAdapter(scisa);
		lvListCate.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View v,
					int position, long id) {
				// check if is context then show context
				// else then show fragment sentences
				Category cateClicked = (Category) mCategoryContainSubCate.get(position);
				// update lai UI cai list luon
				scisa.setmCategorySubSelected(cateClicked);
				mCallback.onSubCategorySelected(cateClicked);
			}
		});
		return view;
	}
	@Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception.
        try {
            mCallback = (OnSubCategorySelectedListener) activity;
            
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }
	/**
	 * @return the mCategoryContainSubCate
	 */
	public Category getmCategoryContainSubCate() {
		return mCategoryContainSubCate;
	}
	/**
	 * @param mCategoryContainSubCate the mCategoryContainSubCate to set
	 */
	public void setmCategoryContainSubCate(Category mCategoryContainSubCate) {
		this.mCategoryContainSubCate = mCategoryContainSubCate;
	}

}
