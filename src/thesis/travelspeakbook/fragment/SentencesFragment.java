package thesis.travelspeakbook.fragment;

import java.util.ArrayList;
import java.util.List;

import model.MySentence;
import thesis.travelspeakbook.R;
import thesis.travelspeakbook.adapter.SentenceAdapter;
import thesis.travelspeakbook.current_data.Constant;
import android.app.Activity;
import android.support.v4.app.ListFragment;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;

public class SentencesFragment extends ListFragment {
	List<MySentence> mSentences;
	String mTextSearch;
	OnSentenceSelectedListener mCallback;
	
	public SentencesFragment(List<MySentence> sentences, String textSearch) {
		mSentences = sentences;
		mTextSearch = textSearch;
	}
	public SentencesFragment() {
		mTextSearch = "";
		mSentences = new ArrayList<MySentence>();
	}
	
	public void updateData(List<MySentence> sentences, String textSearch) {
		mSentences = sentences;
		mTextSearch = textSearch;
		updateList();
	}
	void updateList() {
		setListAdapter(new SentenceAdapter(Constant.CURRENT_ACTIVITY, mSentences, mTextSearch));
	}

    // The container Activity must implement this interface so the frag can deliver messages
    public interface OnSentenceSelectedListener {
        /** Called by HeadlinesFragment when a list item is selected */
        public void onSentenceSelected(MySentence sentenceSelected);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Create an array adapter for the list view, using the Ipsum headlines array
        updateList();
    }

    @Override
    public void onStart() {
        super.onStart();

        // When in two-pane layout, set the listview to highlight the selected list item
        // (We do this during onStart because at the point the listview is available.)
        if (getFragmentManager().findFragmentById(R.id.edit_sentence_fragment) != null) {
            getListView().setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception.
        try {
            mCallback = (OnSentenceSelectedListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        // Notify the parent activity of selected item
    	mCallback.onSentenceSelected(mSentences.get(position));
        
        // Set the item as checked to be highlighted when in two-pane layout
        getListView().setItemChecked(position, true);
    }
    
    public void showMoreItem() {
    	// TODO: code this
    }

	/**
	 * @return the mTextSearch
	 */
	public String getmTextSearch() {
		return mTextSearch;
	}

	/**
	 * @param mTextSearch the mTextSearch to set
	 */
	public void setmTextSearch(String mTextSearch) {
		this.mTextSearch = mTextSearch;
	}

	/**
	 * @return the mSentences
	 */
	public List<MySentence> getmSentences() {
		return mSentences;
	}

	/**
	 * @param mSentences the mSentences to set
	 */
	public void setmSentences(List<MySentence> mSentences) {
		this.mSentences = mSentences;
	}
}
