package thesis.travelspeakbook.fragment;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import model.Category;
import model.Item;
import model.Item.Language;
import model.MySentence;
import model.visitor.CategorySearchVisitor;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import sound.ExtAudioRecorder;
import thesis.travelspeakbook.R;
import thesis.travelspeakbook.current_data.Constant;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.speech.RecognizerIntent;
import android.speech.tts.TextToSpeech;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

public class SearchBarFragment extends Fragment {
	static EditText _etSearch;
	static List<MySentence> _recommend_sentences;
	static Activity _activity;
	protected static final int RESULT_SPEECH = 1;
	SearchAsyncTask _searchTask = new SearchAsyncTask();
	ImageView _ivContext;
	TextView _tvCurrentContext;

	ImageButton _ibClearSentence, _ibInputVoiceButton;
	ProgressBar _pbLoading;

	TextToSpeech _talker;

	OnSearchFinishedListener mCallback;
	Category mCate;
	String mTextSearch;

	public SearchBarFragment(Category cate, String textSearch) {
		mCate = cate;
		mTextSearch = textSearch;
	}
	
	// The container Activity must implement this interface so the frag can
	// deliver messages
	public interface OnSearchFinishedListener {
		/** Called by HeadlinesFragment when a list item is selected */
		public void onSearchFinished(List<MySentence> sentencesFound,
				String textSearch);
	}

	
	public void onAttach(Activity activity) {
		super.onAttach(activity);

		// This makes sure that the container activity has implemented
		// the callback interface. If not, it throws an exception.
		try {
			mCallback = (OnSearchFinishedListener) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString()
					+ " must implement OnHeadlineSelectedListener");
		}
	}

	
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater
				.inflate(R.layout.fragment_search, container, false);

		_etSearch = (EditText) view.findViewById(R.id.editTextSearch);
		_ibClearSentence = (ImageButton) view
				.findViewById(R.id.imageButtonClearSentence);
		_ibInputVoiceButton = (ImageButton) view
				.findViewById(R.id.imageButtonVoice);
		_pbLoading = (ProgressBar) view.findViewById(R.id.progressBarLoading);

		_pbLoading.setVisibility(View.INVISIBLE);

		_etSearch.addTextChangedListener(new TextWatcher() {

			
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// check if input is empty then show basic sentence
				// ParseStatistic.NUM_MAX_OUTPUT =
				// ParseStatistic.NUM_MAX_OUTPUT_DEFAULT;
				

			}

			
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {

			}

			
			public void afterTextChanged(Editable s) {
				turnOffSearchAsync();
				
				mTextSearch = s.toString();
				if (mTextSearch.equals("")) {
					
					List<MySentence> topSentences = (List<MySentence>)mCate
							.accept(new CategorySearchVisitor(mTextSearch));
					mCallback.onSearchFinished(topSentences, mTextSearch);
					return;
				}
				searchAsyncText();
			}


			private void turnOffSearchAsync() {
				Log.d("test", "cancel search task async for category");
				_searchTask.cancel(true);
				_pbLoading.setVisibility(View.INVISIBLE);
			}
		});

		_ibClearSentence.setOnClickListener(new OnClickListener() {

			
			public void onClick(View v) {
				// delete sentence trong edit box roi focus con tro chuot vao do
				// roi danh
				_etSearch.setText("");
				_etSearch.setSelection(0);
			}
		});

		_ibInputVoiceButton.setOnClickListener(new OnClickListener() {

			
			public void onClick(View v) {

				Intent intent = new Intent(
						RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
				
				if (Constant.getLANGMODE_INPUT() == Language.ENG)
					intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, "en-US");
				else if (Constant.getLANGMODE_INPUT() == Language.VIET) 
					intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, "vi");

				try {
					startActivityForResult(intent, RESULT_SPEECH);
				} catch (ActivityNotFoundException a) {
					Toast t = Toast.makeText(v.getContext(),
							"Opps! Your device doesn't support Speech to Text",
							Toast.LENGTH_SHORT);
					t.show();
				}
				// getInputVoice();

			}
		});
		_etSearch.setText(mTextSearch);
		return view;
	}

	private void searchAsyncText() {
		Log.d("test", "create new search task async");
		_pbLoading.setVisibility(View.VISIBLE);
		_searchTask = new SearchAsyncTask();
		_searchTask.execute();
	}

	private static ExtAudioRecorder extAudioRecorder;

	void getInputVoice() {
		String filename = Environment.getExternalStorageDirectory()
				+ "/temp.wav";

		// thu am
		// gui am cho server
		// paste text vao
		// Start recording
		// ExtAudioRecorder extAudioRecorder =
		// ExtAudioRecorder.getInstanse(true); // Compressed recording (AMR)

		extAudioRecorder = ExtAudioRecorder.getInstanse(false); // Uncompressed
																// recording
																// (WAV)
		extAudioRecorder.reset();
		extAudioRecorder.setOutputFile(filename);
		extAudioRecorder.prepare();
		extAudioRecorder.start();

		Toast.makeText(getActivity(), "Start record", Toast.LENGTH_LONG).show();
		_ibClearSentence.setOnClickListener(new OnClickListener() {

			
			public void onClick(View v) {
				Toast.makeText(v.getContext(), "Stop record", Toast.LENGTH_LONG)
						.show();
				stopRecording();
			}
		});

	}

	private void stopRecording() {
		// Stop recording
		extAudioRecorder.stop();
		extAudioRecorder.release();
	}

	public void say(String text2say) {
		_talker.speak(text2say, TextToSpeech.QUEUE_ADD, null);
	}

	
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		switch (requestCode) {
		case RESULT_SPEECH: {
			if (resultCode == android.app.Activity.RESULT_OK && null != data) {

				ArrayList<String> text = data
						.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);

				setmTextSearch(text.get(0));
				_etSearch.setText(mTextSearch);
				searchAsyncText();
			}
			break;
		}
		}
	}

	public void updateCate(Category cate) {
		mCate = cate;
		setmTextSearch("");
		_etSearch.setText(mTextSearch);
	}

	class SearchAsyncTask extends AsyncTask<Object, Integer, List<MySentence>> {
		protected List<MySentence> doInBackground(Object... params) {
//			List<MySentence> foundSentences = (List<MySentence>)mCate.accept(new CategorySearchVisitor(mTextSearch));
			//TODO: search implicit root cate
//			if (foundSentences.size() == 0) {
//				// search with context bigger if context hien tai khac root
//				
//				// if lai tiep tuc size == 0 thi lai kiem bigger context hien tai
//				
//				// search all context if this context is not root
//				if (mCate != Constant.CURRENT_ROOT_CATE) {
//					// search root
//					foundSentences = (List<MySentence>) Constant.CURRENT_ROOT_CATE
//					.accept(new CategorySearchVisitor(mTextSearch));
//				}
//			}
			
			// VERSION 1: search recur in category from small to bigger
			return searchRecur(mCate);
			
			// VERSION 2: just use search root
//			return (List<MySentence>)Constant.CURRENT_ROOT_CATE.accept(new CategorySearchVisitor(mTextSearch));
			
		}
		
		boolean isBelongToCommonWord(String text) {
			int valueLangSearch = Constant.getValueLangSearch(text);
			for (Item item : Constant.commonWord) {
				if (text.toLowerCase().equals(item.getmText()[valueLangSearch])) {
					return true;
				}
			}
			return false;
		}
		List<MySentence> searchRecur(Category currentCate) {
			List<MySentence> foundSentences = (List<MySentence>)currentCate.accept(new CategorySearchVisitor(mTextSearch));
			
			// search outside when %matching very low or have common word
			if (foundSentences.size() > 0) {
				// check common word
				
				MySentence s = foundSentences.get(0);
				int valueLangInput = Constant.getValueLangSearch(mTextSearch);
				// check each keyword in input
				String[] keywords = mTextSearch.split(" ");
				int sizeKeywordsWholdWord = keywords.length;
				int removeKeywordMatchCount = 0;
				if (mTextSearch.charAt(mTextSearch.length()-1) != ' ') {
					String keywordContains = keywords[sizeKeywordsWholdWord-1];
					sizeKeywordsWholdWord--;
					if (isBelongToCommonWord(keywordContains)) {
						// search whole word
						if (Pattern
								.compile(Pattern.quote(keywordContains),
										Pattern.CASE_INSENSITIVE | Pattern.UNICODE_CASE)
								.matcher(
										s.getmText()[valueLangInput]).find()) {
							removeKeywordMatchCount++;
						}
					}
						
				}
				for (int i = 0; i<sizeKeywordsWholdWord; i++) {
					if (isBelongToCommonWord(keywords[i])) {
						// check xem coi co' match hay khong
						// neu match thi lai tang tip
						String patternString = "\\b(" + keywords[i] + ")\\b";
						Pattern pattern = Pattern.compile(patternString, Pattern.CASE_INSENSITIVE | Pattern.UNICODE_CASE);
						Matcher matcher = pattern.matcher(s.getmText()[valueLangInput]);
			
						while (matcher.find()) {
			//			    System.out.println(matcher.group(1));
						    removeKeywordMatchCount++;
						}
					}
				}
				// tinh so tu dung'
				// lay %match - so tu match < 1 then continue search outside
				float remainPercent = s.getmPercentMatch() - removeKeywordMatchCount;
				// TODO: co the show cai nay ra tam thoi truoc neu o ngoai khong co
				if (remainPercent > 1)
					return foundSentences;
			}
						
			// size == 0 ==> still find bigger cate if this cate is not null
			Category biggerCate = currentCate.getmBelongCate();
			if (biggerCate == null) 
				return foundSentences;
			return searchRecur(biggerCate);
		}

		protected void onProgressUpdate(Integer... progress) {
			// setProgressPercent(progress[0]);
		}

		protected void onPostExecute(List<MySentence> result) {
			// showDialog("Downloaded " + result + " bytes");
			// add vao variable of autocomplete box
			_pbLoading.setVisibility(View.INVISIBLE);
			_recommend_sentences = result;
			// update fragment sentences
			mCallback.onSearchFinished(_recommend_sentences, mTextSearch);
		}
	}

	class RequestTask extends AsyncTask<String, String, String> {

		
		protected String doInBackground(String... uri) {
			HttpClient httpclient = new DefaultHttpClient();
			HttpResponse response;
			String responseString = null;
			try {
				response = httpclient.execute(new HttpGet(uri[0]));
				StatusLine statusLine = response.getStatusLine();
				if (statusLine.getStatusCode() == HttpStatus.SC_OK) {
					ByteArrayOutputStream out = new ByteArrayOutputStream();
					response.getEntity().writeTo(out);
					out.close();
					responseString = out.toString();
				} else {
					// Closes the connection.
					response.getEntity().getContent().close();
					throw new IOException(statusLine.getReasonPhrase());
				}
			} catch (ClientProtocolException e) {
			} catch (IOException e) {
			}
			return responseString;
		}

		
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			// Do anything with response..
		}
	}

	/**
	 * @return the mTextSearch
	 */
	public String getmTextSearch() {
		return mTextSearch;
	}

	/**
	 * @param mTextSearch
	 *            the mTextSearch to set
	 */
	public void setmTextSearch(String mTextSearch) {
		this.mTextSearch = mTextSearch;
	}
}
