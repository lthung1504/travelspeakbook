package thesis.travelspeakbook;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import model.Category;
import model.ConvertUnsigned;
import model.Item;
import model.Item.Language;
import model.MySentence;
import model.PositionTag;
import model.TagPosComparator;
import model.TagSentence;
import model.visitor.CategoryFindSentenceVisitor;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import sound.SoundPoolPlayer;
import thesis.travelspeakbook.adapter.ActivitySwipeDetector;
import thesis.travelspeakbook.current_data.Constant;
import thesis.travelspeakbook.custom_view.FavoriteIconImageButton;
import thesis.travelspeakbook.custom_view.FlowLayout;
import thesis.travelspeakbook.custom_view.TagChooseTextView;
import thesis.travelspeakbook.custom_view.TagOptionTextView;
import thesis.travelspeakbook.fragment.EmptyFragment;
import thesis.travelspeakbook.fragment.InputDateBoardFragment;
import thesis.travelspeakbook.fragment.InputDateBoardFragment.OnKeyDateSelectedListener;
import thesis.travelspeakbook.fragment.InputNumberBoardFragment;
import thesis.travelspeakbook.fragment.InputNumberBoardFragment.OnKeyNumberSelectedListener;
import thesis.travelspeakbook.fragment.InputTagChooseFragment;
import thesis.travelspeakbook.fragment.InputTagChooseFragment.OnTagSelectedListener;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.speech.tts.TextToSpeech;
import android.speech.tts.TextToSpeech.OnInitListener;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.text.Editable;
import android.text.Html;
import android.text.TextUtils.TruncateAt;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
public class EditActivity extends FragmentActivity implements OnKeyDateSelectedListener, OnKeyNumberSelectedListener, OnTagSelectedListener{
	public static final String OPEN_BRACKET_TAG = "[";
	// STORAGE
	SharedPreferences mMySharedPreferences;
	SharedPreferences.Editor mMyEditor;

	MySentence mSentence;
//	ListView _lvTagRecommends;
	
	Activity mActivity;
	// list textviews
	ImageView _ivCurrentContext;
	
	// edit text are using
	EditText _etUsing;
	
	ImageButton _ibInputVoiceButton, _ibSpeakerButton;
	Button _btnBack;
	EditText _etTextSearch;
	FavoriteIconImageButton _fiibFavorite;
	
	List<MySentence> _recommend_sentences;
	TextView _tvOriginal, _tvOutput, _tvCurrentContext;
	ProgressBar _pbWaitingDownload;
	
	String mExtraText = "";
	
	//TTS
	TextToSpeech _talker;
	
	
	public void say(String text2say) {
		_talker.speak(text2say, TextToSpeech.QUEUE_ADD, null);
	}
	
	public void sayVietnamese(String text2say) {
		// check file exists or not
		File file = new File(directoryFileWavVietnam(text2say));
		if (file.exists()) {
			// say
			SoundPoolPlayer spp = new SoundPoolPlayer(this);
			spp.playWave(file);
			return;
		}
		// download if don't have in cached
		if (!isOnline()) {
			Toast.makeText(this, "Cannot complete action without a data connection", Toast.LENGTH_LONG).show();
			return;
		}
		showWaitingDialog();
		DownloadVietnameseAsyncTask dvat = new DownloadVietnameseAsyncTask();
		dvat.execute(text2say);
	}
	
	public boolean isOnline() {
	    ConnectivityManager cm =
	        (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
	    NetworkInfo netInfo = cm.getActiveNetworkInfo();
	    if (netInfo != null && netInfo.isConnectedOrConnecting()) {
	        return true;
	    }
	    return false;
	}

	List<String> takeKeyword(String line) {
		if (line.contains(OPEN_BRACKET_TAG) == false) {
			return new ArrayList<String>();
		}
		List<String> results = new ArrayList<String>();
		Pattern MY_PATTERN = Pattern.compile("(\\[)(.*?)(\\])");
		Matcher m = MY_PATTERN.matcher(line);
		while (m.find()) {
			String s = m.group(0);
//			s = s.substring(1, s.length() - 1);
			results.add(s);
		}
		return results;
	}
	boolean isATagSentence(String input) {
		if (input.contains(OPEN_BRACKET_TAG))
			return true;
		return false;
	}
	ProgressDialog waitingDialog;
	void showWaitingDialog() {
		waitingDialog = ProgressDialog.show(EditActivity.this, "", 
                "Downloading sound. Please wait...", true);
	}

	Category getCateBig(Category c) {
		// check whether this cate has parent cate is root cate
		Category parent = c.getmBelongCate();
		if (parent == null) 
			return c;
		if (parent == Constant.CURRENT_ROOT_CATE) 
			return c;
		
		return getCateBig(c.getmBelongCate());
	}
	
	void setUpUI() {
		_btnBack.setText(Constant.RES_BACK_LIST[Constant.getLANGMODE_INPUT().value]);
		_btnBack.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				onBackPressed();
			}
		});
		_talker = new TextToSpeech(this, new OnInitListener() {

			public void onInit(int status) {
				// empty but must have, can not understand this thing
			}
		});
		
		_ibSpeakerButton.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				speakFunction();
			}

			
		});
		Button btnSpeak = (Button) findViewById(R.id.buttonSpeak);
		btnSpeak.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				speakFunction();
			}
		});
		btnSpeak.setText(Constant.RES_SPEAK[Constant.getLANGMODE_INPUT().value]);
		
		Category bigCate = getCateBig(mSentence.getmBelongCate());
		_tvCurrentContext.setText(bigCate.getmText()[Constant.getLANGMODE_INPUT().value]);
		_ivCurrentContext.setImageResource(Constant.RES_ID_SMALL_ICON_CATE.get(bigCate));
		
		TextView tvCurrentLang = (TextView) findViewById(R.id.textViewCurrentLang);
		tvCurrentLang.setText(Constant.RES_SHORTHAND_LANG[Constant.getLANGMODE_OUTPUT().value]);
		
		fillOriginalAndOutput();
	}
	private void speakFunction() {
		String text = mSentence.getmText()[Constant.getLANGMODE_OUTPUT().value];
		if (Constant.getLANGMODE_OUTPUT()== Language.ENG) {
			say(text);
		}
		else if (Constant.getLANGMODE_OUTPUT() == Language.VIET) {
			sayVietnamese(text);
		}
	}
	private void fillOriginalAndOutput() {
		if (mSentence.getmPattern() == null) {
			_tvOriginal.setText(mSentence.getmText()[Constant.getLANGMODE_INPUT().value]);
			_tvOutput.setText(mSentence.getmText()[Constant.getLANGMODE_OUTPUT().value]);
		}
		else { 
			String textOutput = updateSentenceWithStringOptional(Constant.getLANGMODE_OUTPUT().value);
			_tvOutput.setText(Html.fromHtml(textOutput));
			// check xem co' tag nao khong thi moi' lam, ==> co' pattern hay khong
			setUpLineOriginEdit();	
		}
	}
	void onChangeTextTagOptional(Item selectedTagItem, String inputed) {
		selectedTagItem.setData(new String[] {inputed, inputed, inputed, inputed});
		mSentence.refreshData();
	}
	private void setUpLineOriginEdit() {
		List<PositionTag> tags = new ArrayList<PositionTag>();
		List<PositionTag> tag1 = mSentence.getmPattern().getmSemiPatterns()[Constant.getLANGMODE_INPUT().value].getmPosTags();
		List<PositionTag> tag2 = mSentence.getmPattern().getmSemiPatterns()[Constant.getLANGMODE_INPUT().value].getmPosTagsManualFill();
		tags.addAll(tag1);
		tags.addAll(tag2);
		Collections.sort(tags, new TagPosComparator());// take list each tag
		// duyet tung tag de tao theo kieu phia' cuoi'
		String textPatternInput = mSentence.getmPattern().getmText()[Constant.getLANGMODE_INPUT().value];
		int curPos = 0;
		List<Item> tagChooseSelectedItems = mSentence.getmTagChooseSelectedItems();
		int countIndexTagChoose = 0;
		int countIndexTagOptional = 0;
		FlowLayout ll = (FlowLayout)findViewById(R.id.flowLayoutTextOriginalLines);
		ll.removeAllViewsInLayout(); // refresh layout
		
		ll.setVisibility(View.VISIBLE);
		for (final PositionTag pt: tags) {
			// take pos again vi phai get replace
			String sign = pt.getmTag().getmSign();
			int pos = textPatternInput.indexOf(sign);
			textPatternInput = textPatternInput.replaceFirst(sign, "");
			
			String head = textPatternInput.substring(curPos, pos);
			// create text view for head
			TextView tv = new TextView(this);
			tv = setTextStyleTranslate(tv);
			tv.setText(head);
			// add to layout
			ll.addView(tv);
			// create textview for tag
			
			
			// check whether this tag is choose or option
			if (pt.getmTag().ismIsTagOptional()) {// option tag
				final Item tagSelectedItem = mSentence.getmTagOptionalSelectedItems().get(countIndexTagOptional);
				
				if (isUseNumKeyboard()) {
					final TagOptionTextView totvNum = new TagOptionTextView(this, pt.getmTag(), countIndexTagOptional, tagSelectedItem);
					setUpStyleEditBoxTag(totvNum);
					ll.addView(totvNum);
					totvNum.setOnFocusChangeListener(new OnFocusChangeListener() {
						
						public void onFocusChange(View v, boolean hasFocus) {
							setUpStyleFocusEditBox(v, hasFocus);
							if (hasFocus)
								Constant.showFragment(getSupportFragmentManager(), new InputNumberBoardFragment(totvNum), R.id.fragment_extra_input, false);
						}
					});
				}
				else if (isUseDateKeyboard()) {
					final TagOptionTextView totvDate = new TagOptionTextView(this, pt.getmTag(), countIndexTagOptional, tagSelectedItem);
					setUpStyleEditBoxTag(totvDate);
					
					ll.addView(totvDate);

					// show fragment already
					totvDate.setOnFocusChangeListener(new OnFocusChangeListener() {
						
						public void onFocusChange(View v, boolean hasFocus) {
							setUpStyleFocusEditBox(v, hasFocus);
							if (hasFocus)
								Constant.showFragment(getSupportFragmentManager(), new InputDateBoardFragment(totvDate), R.id.fragment_extra_input, false);	
						}
					});
				}
				else {	// danh cho truong hop use text keyboard
					final EditText totvText = new EditText(this);
					String textCache = mSentence.getmTagOptionalSelectedItems().get(countIndexTagOptional).getmText()[Constant.getLANGMODE_INPUT().value];
					totvText.setText(textCache);
					
					totvText.setBackgroundResource(R.drawable.input_box);
					totvText.setTextSize(getResources().getDimension(R.dimen.text_size_show));
					totvText.setSingleLine(true);
					
//					totvText.setFocusable(true);
//					totvText.setFocusableInTouchMode(true);
//					totvText.setClickable(true);
					totvText.setSelectAllOnFocus(true);
					ll.addView(totvText);
					totvText.setOnClickListener(new OnClickListener() {
						
						public void onClick(View v) {
							((EditText)v).selectAll();
						}
					});
					
					totvText.setOnFocusChangeListener(new OnFocusChangeListener() {
						
						public void onFocusChange(View v, boolean hasFocus) {
							setUpStyleFocusEditBox(v, hasFocus);
//							if (hasFocus) {
//								Constant.showFragment(getSupportFragmentManager(), new EmptyFragment(), R.id.fragment_extra_input, false);
//							}
							
						}
					});
					totvText.addTextChangedListener(new TextWatcher() {
						
						public void onTextChanged(CharSequence s, int start, int before, int count) {
							// TODO: update text translate
							// truoc mat la input gi thi ra nay^' di
							// sau nay se dung google dich de translate
							String text = s.toString();
							Item item = new Item(new String[] {text, text, text, text});
							String textOut = updateSentenceWithStringOptional(item.getmText()[Constant.getLANGMODE_OUTPUT().value], pt.getmTag(), Constant.getLANGMODE_OUTPUT().value);
							_tvOutput.setText(Html.fromHtml(textOut));
							onChangeTextTagOptional(tagSelectedItem, text);
						}
						
						
						public void beforeTextChanged(CharSequence s, int start, int count,
								int after) {
							
						}
						
						public void afterTextChanged(Editable s) {
							
						}
					});
				}
				countIndexTagOptional++;				
			} 
			else {
				final TagChooseTextView tctv = new TagChooseTextView(this);
				setUpStyleEditBoxTag(tctv);
				tctv.setData(pt.getmTag(), tagChooseSelectedItems.get(countIndexTagChoose), countIndexTagChoose);
				ll.addView(tctv);
				
				tctv.setOnFocusChangeListener(new OnFocusChangeListener() {
					
					public void onFocusChange(View v, boolean hasFocus) {
						setUpStyleFocusEditBox(v, hasFocus);
						if (hasFocus)
							Constant.showFragment(getSupportFragmentManager(), new InputTagChooseFragment(tctv), R.id.fragment_extra_input, false); 
					}

					
				});
				
				countIndexTagChoose++;
			}
			curPos = pos;
		}
		String rest = textPatternInput.substring(curPos, textPatternInput.length());
		TextView tv = new TextView(this);
		tv = setTextStyleTranslate(tv);
		tv.setText(rest);
		ll.addView(tv);
		_tvOriginal.setVisibility(View.GONE);
	}
	
	private void setUpStyleFocusEditBox(View v, boolean hasFocus) {
		if (hasFocus) {
			v.setBackgroundResource(R.drawable.input_box_active);
		}
		else
			v.setBackgroundResource(R.drawable.input_box);
	}
	
	private void setUpStyleEditBoxTag(final TextView totvText) {
		totvText.setBackgroundResource(R.drawable.input_box);
		totvText.setTextSize(getResources().getDimension(R.dimen.text_size_show));
		totvText.setSingleLine(true);
		
		totvText.setFocusable(true);
		totvText.setFocusableInTouchMode(true);
		totvText.setClickable(true);
		totvText.setEllipsize(TruncateAt.MARQUEE);
		
//		android:singleLine="true" 
//		        android:ellipsize="marquee"
//		        android:marqueeRepeatLimit ="marquee_forever"
//		        android:focusable="true"
//		        android:focusableInTouchMode="true" 
//		        android:scrollHorizontally="true"
//		        android:layout_width="wrap_content" 
//		        android:layout_height="wrap_content"/>
	}

	private String fillTextChooseTag(int valueLang) {
		String textPattern = mSentence.getmPattern().getmText()[valueLang];
		List<Item> tagChooseSelectedItems = mSentence.getmTagChooseSelectedItems();
		List<PositionTag> chooseTags = mSentence.getmPattern().getmSemiPatterns()[valueLang].getmPosTags();
		for (int i = 0; i<chooseTags.size(); i++) {
			PositionTag pt = chooseTags.get(i);
			String sign = pt.getmTag().getmSign();
			String text = tagChooseSelectedItems.get(i).getmText()[valueLang];
			
			text = Constant.decorTextTagInput(text);
			textPattern = textPattern.replace(sign, text);
		}
		return textPattern;
	}
	String updateSentenceWithStringOptional(String textOptionalTag, TagSentence tsOptional, int valueLang) {
		String textPattern = fillTextChooseTag(valueLang);
		return textPattern.replaceFirst(tsOptional.getmSign(), Constant.decorTextTagOptinal(textOptionalTag));
	}
	
	String updateSentenceWithStringOptional(int valueLang) {
		String textPattern = fillTextChooseTag(valueLang);
		
		List<Item> tagOptionalSelectedItems = mSentence.getmTagOptionalSelectedItems();
		List<PositionTag> optionalTags = mSentence.getmPattern().getmSemiPatterns()[valueLang].getmPosTagsManualFill();
		for (int i = 0; i<optionalTags.size(); i++) {
			PositionTag pt = optionalTags.get(i);
			String sign = pt.getmTag().getmSign();
			String text = tagOptionalSelectedItems.get(i).getmText()[valueLang];
			
			text = Constant.decorTextTagInput(text);
			textPattern = textPattern.replace(sign, text);
		}
		return textPattern;
	}
	boolean isUseDateKeyboard() {
		return check(Constant.TAGS_USE_DATE_KEYBOARD);
	}
	boolean isMember(String text, String[] list) {
		for (String item : list) {
			if (item.equals(text)) 
				return true;
		}
		return false;
	}
	
	boolean isUseNumKeyboard() {
		return check(Constant.TAGS_USE_NUM_KEYBOARD);
	}

	private boolean check(String[] list) {
		// check in sentences - optional tags
		List<PositionTag> tags = mSentence.getmPattern().getmSemiPatterns()[0].getmPosTagsManualFill();
		for (PositionTag tag: tags) {
			String keyTag = tag.getmTag().getmKey();
			return isMember(keyTag, list);
		}
		return false;
	}
	boolean isUseTextKeyboard() {
		if (!isUseDateKeyboard() && !isUseNumKeyboard())
			return true;
		return false;
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		Log.d("test", "Edit activity");
		super.onCreate(savedInstanceState);
		
		overridePendingTransition(R.anim.transition_right_to_left,
				R.anim.transition_middle_to_left);
		setUpVar();
		setUpUI();
		
	}

//	private void setUpTextKeyboard() {
//		final EditText et = (EditText) findViewById(R.id.editTextEdit);
//		et.setVisibility(View.VISIBLE);
//		InputMethodManager imm = (InputMethodManager) getBaseContext().getSystemService(Context.INPUT_METHOD_SERVICE);
//		imm.showSoftInput(et, InputMethodManager.SHOW_IMPLICIT);
//		et.addTextChangedListener(new TextWatcher() {
//			
//			public void onTextChanged(CharSequence s, int start, int before, int count) {
//				replaceTagDecorated(et.getText().toString());
//			}
//			
//			public void beforeTextChanged(CharSequence s, int start, int count,
//					int after) {
//				
//			}
//			
//			public void afterTextChanged(Editable s) {
//				
//			}
//		});
//	}

	private TextView setTextStyleTranslate(TextView tvTemp) {
		tvTemp.setTextColor(getResources().getColor(R.color.text_color_origin));
		tvTemp.setTextSize(getResources().getDimension(R.dimen.text_size_show));
		
		return tvTemp;
//		<TextView
//        android:id="@+id/textViewOriginal"
//        android:layout_width="0dp"
//        android:layout_height="wrap_content"
//        android:layout_gravity="center_vertical"
//        android:layout_marginLeft="@dimen/margin_left_size_text"
//        android:layout_weight="6"
//        android:paddingLeft="@dimen/margin_left_size_text"
//        android:text="TextView"
//        android:textAppearance="?android:attr/textAppearanceMedium"
//        android:textColor="#aaaaaa"
//        android:textSize="10pt" />

	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		overridePendingTransition(R.anim.transition_left_to_right,
				R.anim.transition_middle_to_right);
	}

	void setUpStorage() {
		mMySharedPreferences = getSharedPreferences(Constant.TRAVEL_STORAGE, 0);
		mMyEditor = mMySharedPreferences.edit();
		
	}
	void setUpVar() {
		setUpStorage();
		setContentView(R.layout.page_translate);
		_fiibFavorite = (FavoriteIconImageButton) findViewById(R.id.customFavoriteButton);
		_tvOriginal = (TextView) findViewById(R.id.textViewOriginal);
		_tvOutput = (TextView) findViewById(R.id.textViewTranslate);
		_tvCurrentContext = (TextView) findViewById(R.id.textViewCurrentContext);
		_ivCurrentContext = (ImageView) findViewById(R.id.imageViewCurrentContext);
		_etTextSearch = (EditText) findViewById(R.id.editTextSearch);
		_btnBack = (Button) findViewById(R.id.buttonBack);
		_ibSpeakerButton = (ImageButton) findViewById(R.id.imageButtonSpeaker);

		setmSentence(Constant.CURRENT_SENTENCE);
	}	

	class IndexInfor {
		int index;
		String info;
		boolean isTag;
		public IndexInfor(int index, String info, boolean isTag) {
			this.index = index;
			this.info = info;
			this.isTag = isTag;
		}
	}
	class IndexInforComparator implements Comparator<IndexInfor> {
		public int compare(IndexInfor lhs, IndexInfor rhs) {
			if (lhs.index < rhs.index) 
				return -1;
			else if (lhs.index == rhs.index) 
				return 0;
			else
				return 1;
		}
	}
	void addGestureDectec() {
		ActivitySwipeDetector activitySwipeDetector = new ActivitySwipeDetector(this);
		RelativeLayout rlMain = (RelativeLayout) findViewById(R.id.RelativeLayoutMain);
		rlMain.setOnTouchListener(activitySwipeDetector);
	}
	
	private String directoryFileWavVietnam(String noidung) {
		return Environment.getExternalStorageDirectory() + "/" + createNameFileWav(noidung);
	}
    
	private class DownloadVietnameseAsyncTask extends AsyncTask<String, Void, Void> {
        @Override
        protected Void doInBackground(String... params) {
        	calculate(params[0]);
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
//        	_pbWaitingDownload.setVisibility(View.INVISIBLE);
        	waitingDialog.dismiss();
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected void onProgressUpdate(Void... values) {
        }

    }
	private static final String SOAP_ACTION = "http://tempuri.org/TTS";
    private static final String METHOD_NAME = "TTS";
    private static final String NAMESPACE = "http://tempuri.org/";
    private static final String URL = "http://www.ailab.hcmus.edu.vn/tts_bong_demo/Service.asmx";
    
	@SuppressLint("NewApi")
	public void calculate(String noidung) 
    {
        
        try { 
            SoapObject Request = new SoapObject(NAMESPACE, METHOD_NAME);
            Request.addProperty("czt", "truongdeptrai");
            Request.addProperty("noidung", noidung);
            
            SoapSerializationEnvelope soapEnvelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            soapEnvelope.dotNet = true;
            soapEnvelope.setOutputSoapObject(Request);

            HttpTransportSE transport= new HttpTransportSE(URL);

            transport.call(SOAP_ACTION, soapEnvelope);
            SoapPrimitive resultString = (SoapPrimitive)soapEnvelope.getResponse();

            String encode = resultString.toString();
            
            // decoded
            byte[] decoded = Base64.decode(encode, 0);

            try
            {
                File file2 = new File(directoryFileWavVietnam(noidung));
//                File file2 = new File(Environment.getDataDirectory() + "/hello-5.wav");
                FileOutputStream os = new FileOutputStream(file2, true);
                os.write(decoded);
                os.close();
                
                // play file
                SoundPoolPlayer spp = new SoundPoolPlayer(this);
                spp.playWave(file2);
            }
            catch (Exception e)
            {
            	Toast.makeText(mActivity.getBaseContext(), "error: " + e.getMessage(), Toast.LENGTH_LONG).show();
            }
            
            
        }
        catch(Exception ex) {
            Toast.makeText(mActivity.getBaseContext(), "Error: " + ex.getMessage(), Toast.LENGTH_LONG).show();
        }
        
    }

	String createNameFileWav(String text2say) {
    	ConvertUnsigned cu = new ConvertUnsigned();
    	String fileName = cu.ConvertString(text2say);
    	return fileName + ".wav";
    }
	public void onKeyDateSelected(String textInputed, String textOrigin, TagOptionTextView totvModify) {
		String textOutput = updateSentenceWithStringOptional(textInputed, totvModify.getmTag(), Constant.getLANGMODE_OUTPUT().value);
		_tvOutput.setText(Html.fromHtml(textOutput));
		totvModify.setText(textOrigin);
		
		onChangeTextTagOptional(totvModify.getmSelectedTagItem(), textInputed);
	}
	public void onTagItemSelected(TagChooseTextView tagChooseTextView, int position) {
		// version 2: kiem cai sentence y chang
		// update cai tag
		tagChooseTextView.requestFocus();
		List<Item> newTagChooseSelected = new ArrayList<Item>();
		List<Item> oldTagChooseSelected = mSentence.getmTagChooseSelectedItems();
		int indexInSenteceToChangeThisTag = tagChooseTextView.getmPositionInSentence();
		Item itemSelected = tagChooseTextView.getmTag().getmItems().get(position);
		
		for (Item item : oldTagChooseSelected) {
			newTagChooseSelected.add(item);
		}
		newTagChooseSelected.set(indexInSenteceToChangeThisTag, itemSelected);
		Category currentCate = mSentence.getmBelongCate();
		
		MySentence resultSentence= (MySentence)currentCate.accept(new CategoryFindSentenceVisitor(mSentence.getmPattern(), newTagChooseSelected));
		Constant.CURRENT_SENTENCE = resultSentence;
		setmSentence(resultSentence);
		// update just the tag have changed and the output
		tagChooseTextView.setText(itemSelected.getmText()[Constant.getLANGMODE_INPUT().value]);
		// update output
		String textOutput = updateSentenceWithStringOptional(Constant.getLANGMODE_OUTPUT().value);
		_tvOutput.setText(Html.fromHtml(textOutput));
		
		
//		TagSentence ts = tagChooseTextView.getmTag();
//		// update len man hinh output va input
//		// replace cai tag nay o input va output
//		// kiem cai sentence y chang vay
//		// update activity nay cho sentence moi'
//		// coi nhu mot lan dung, tao event use cau de tang bien count len
//		// version 1:
//		Category currentCate = _sentence.getmBelongCate();
//		List<Integer> newInts = new ArrayList<Integer>();
//		List<Integer> olds = _sentence.getmPosContentTags();
//		for (Integer tag : olds) {
//			newInts.add(new Integer(tag));
//		}
//		// find index of tag
//		List<PositionTag> tags = _sentence.getmPattern().getmSemiPatterns()[0].getmPosTags();
//		int index = 0;
//		for (int i = 0; i< tags.size(); i++) {
//			if (tags.equals(ts))
//				index = i;
//		}
//		// set 
//		newInts.set(index, position);
//		MySentence resultSentence= (MySentence)currentCate.accept(new CategoryFindSentenceVisitor(_sentence.getmPattern(), newInts));
//		// refresh activity nay
//		// hoac la update lai cai tag nay va text output la duoc thoi
//		_sentence = Constant.CURRENT_SENTENCE = resultSentence;
//		
//		
//		// update just the tag have change and the output
//		tagChooseTextView.setText(ts.getmItems().get(position).getmText()[Constant.getLANGMODE_INPUT().value]);
//		
//		fillOriginalAndOutput();
//		
		
	}
	void onUnFocusForTagOptional(TagSentence ts, String textTyped, int positionTagInSentence) {
		Item item = new Item(new String[] {textTyped, textTyped, textTyped, textTyped});
		Item itemChange = getItemInTagOptional(item, ts);
		
		// update in sentence
		List<Item> tagOptionalSelectedItems = mSentence.getmTagOptionalSelectedItems();
		tagOptionalSelectedItems.set(positionTagInSentence, itemChange);
	}
	public void onKeyNumberSelected(String textInputed, TagOptionTextView totvModify) {
		String textOutput = updateSentenceWithStringOptional(textInputed, totvModify.getmTag(), Constant.getLANGMODE_OUTPUT().value);
		_tvOutput.setText(Html.fromHtml(textOutput));

		totvModify.setText(textInputed);
		
		// TODO: update item tag optional, sau nay se tao them de danh recommend
		
		onChangeTextTagOptional(totvModify.getmSelectedTagItem(), textInputed);
	}
	
	Item getItemInTagOptional(Item typed, TagSentence ts) {
		// duyet xem phai la key khong
		for (int i = 0; i<ts.getmItems().size(); i++) {
			Item item = ts.getmItems().get(i);
			if (item.compareLang(typed, Constant.getLANGMODE_INPUT())) {
				return item;
			}
		}
		// create new item and put to this list tag
		ts.getmItems().add(typed);
		return typed;
	}

	public MySentence getmSentence() {
		return mSentence;
	}

	public void setmSentence(MySentence mSentence) {
		this.mSentence = mSentence;
		this.mSentence.increaseCount();
		_fiibFavorite.setmSentence(mSentence);
	}
	
    
}
