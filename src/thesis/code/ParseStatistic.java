package thesis.code;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.StreamCorruptedException;

import model.DatabaseTravelSpeakBook;
import thesis.travelspeakbook.R;
import thesis.travelspeakbook.current_data.Constant;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.res.Resources.NotFoundException;
import android.util.Log;
import android.widget.Toast;

public class ParseStatistic {
     private static final String TAG = "ParseStatistic";
     public static int NUM_MAX_OUTPUT_DEFAULT = 10;
     public static int NUM_MAX_OUTPUT = NUM_MAX_OUTPUT_DEFAULT;
     private static final int MAX_VALUE_RIGHT_ORDER = 20;
     public String INPUT_FILE_DATABASE = "database.txt";
     
     static Context mContext;

     /**
	 * @return the mContext
	 */
	public static Context getmContext() {
		return mContext;
	}

	/**
	 * @param mContext the mContext to set
	 */
	public static void setmContext(Context mContext) {
		ParseStatistic.mContext = mContext;
	}

	public ParseStatistic(Context context) {
    	 mContext = context;
     }
     
     public static void save_database_from_android(DatabaseTravelSpeakBook database) {
          try
             {
               //Lay phan vung hay file cua SDCard
               ContextWrapper cw = new ContextWrapper(mContext);
               File sdcard = cw.getDir("media", Context.MODE_PRIVATE);

               if(sdcard.canWrite())
               {
                   FileOutputStream fos = mContext.openFileOutput(Constant.DATABASE_SDCARD, Context.MODE_PRIVATE);
                   ObjectOutputStream os = new ObjectOutputStream(fos);
                   os.writeObject(database);
                   os.close();
                   fos.close();
               }
             }
             catch(Exception e)
             {
                  Toast.makeText(mContext,"Error"+e.getMessage(),Toast.LENGTH_SHORT).show();
             }
            }
     
     static public DatabaseTravelSpeakBook getCateFromInternalMem() {
    	 DatabaseTravelSpeakBook result = null;
         try {
              FileInputStream fis = mContext.openFileInput(Constant.DATABASE_SDCARD);
              ObjectInputStream is = new ObjectInputStream(fis);
              result = (DatabaseTravelSpeakBook) is.readObject();
              is.close();
              fis.close();
         } catch (FileNotFoundException e) {
              e.printStackTrace();
         } catch (StreamCorruptedException e) {
              e.printStackTrace();
         } catch (IOException e) {
              e.printStackTrace();
         } catch (ClassNotFoundException e) {
              e.printStackTrace();
         }
         return result;
     }
     static public DatabaseTravelSpeakBook getCategoryFromResource(int resId) {
    	 DatabaseTravelSpeakBook result = null;
         try {
               InputStream is = mContext.getResources().openRawResource(resId);
               ObjectInputStream ois = new ObjectInputStream(is);
               result = (DatabaseTravelSpeakBook) ois.readObject();
               ois.close();
               is.close();
          } catch (NotFoundException e) {
              Log.d(TAG, "file khong tim thay" + e.getMessage());
          } catch (IOException e) {
               Log.d(TAG, "khong doc duoc category deserialize" + e.getMessage());
          } catch (ClassNotFoundException e) {
               e.printStackTrace();
          }
         return result;
     }

     static String pre_processing_input(String input) {
          input = input.replace("'ll", " will");
          input = input.replace("'d", " would");
          input = input.replace("'m", " am");
          input = input.replace("'s", " is");
          input = input.replace("'re", " are");
          input = input.replace("n't", " not");

          return input;
     }


     static public void createDatabaseOnInternalMemory() {
			File file = new File(mContext.getFilesDir(), Constant.DATABASE_SDCARD);
			if (file.exists()) {
				return;
			}
			
			// file have not created
			// install to list
			Constant.DATABASE = ParseStatistic.getCategoryFromResource(R.raw.output);
			// create file to internal memory
			ParseStatistic.save_database_from_android(Constant.DATABASE);
		}
     

}