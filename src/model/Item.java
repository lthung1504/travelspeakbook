package model;

import java.io.Serializable;

public class Item implements Serializable{
	public static final int TOTAL_LANGS = 4;


	private static final long serialVersionUID = 1L;
	
	
	String[] mText = new String[TOTAL_LANGS+1]; // +1 for unsigned vietnam
	// LANGUAGE
	public enum Language {
		ENG(0), VIET(1), LAO(2), CAMPUCHIA(3), VIET_UNSIGNED(4);
		public int value;

		Language(int value) {
			this.value = value;
		}
	}
	
	public Item() {
		String[] texts = new String[] {""};
		setData(texts);
	}
	public Item(String[] texts) {
		setData(texts);
	}
	boolean equal(String name, int positionLang) {
		if (mText[positionLang].equals(name))
			return true;
		return false;
	}
	public void setText(String text, int positionLang) {
		mText[positionLang] = text;
	}
	public boolean compareLang(Item item, Language lang) {
		if (getmText()[lang.value].equals(item.getmText()[lang.value]))
			return true;
		return false;
	}
	/*
	 * check condition ok
	 */
	public void setData(String[] texts) {
		int length = texts.length;
		for (int i = 0; i<length; i++) {
			mText[i] = texts[i];
		}
		for (int i = length; i<TOTAL_LANGS; i++) {
			mText[i] = "";
		}
		
		ConvertUnsigned cu = new ConvertUnsigned();
		mText[Language.VIET_UNSIGNED.value] = cu.ConvertString(mText[Language.VIET.value]);
	}
	public String toString() {
		return mText.toString();
	}

	/**
	 * @return the mText
	 */
	public String[] getmText() {
		return mText;
	}

	/**
	 * @param mText the mText to set
	 */
	public void setmText(String[] mText) {
		this.mText = mText;
	}
}
