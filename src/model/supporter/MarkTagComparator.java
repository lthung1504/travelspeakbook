package model.supporter;

import java.util.Comparator;

public class MarkTagComparator implements Comparator<MarkTag> {
	public int compare(MarkTag s1, MarkTag s2) {
		int compare;
		if (s1.mPos > s2.mPos)
			compare = 1;
		else if (s1.mPos < s2.mPos)
			compare = -1;
		else
			compare = 0;
		return compare;
	}
}
