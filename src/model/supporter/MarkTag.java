package model.supporter;

import java.util.Comparator;

public class MarkTag {
	int mPos;
	String mTagSign;
	public MarkTag(int pos, String tagSign) {
		mPos = pos;
		mTagSign = tagSign;
	}
	public int getmPos() {
		return mPos;
	}
	public void setmPos(int mPos) {
		this.mPos = mPos;
	}
	public String getmTagSign() {
		return mTagSign;
	}
	public void setmTagSign(String mTagSign) {
		this.mTagSign = mTagSign;
	}
}
