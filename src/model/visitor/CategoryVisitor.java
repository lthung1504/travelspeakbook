package model.visitor;

import model.Category;

public interface CategoryVisitor {
	public Object visit(Category c);
}
