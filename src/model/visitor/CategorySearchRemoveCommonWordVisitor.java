package model.visitor;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import model.Category;
import model.ConvertUnsigned;
import model.Item;
import model.MySentence;
import model.SentenceComparator;
import model.SentenceCountComparator;
import model.Item.Language;
import thesis.travelspeakbook.current_data.Constant;
import android.util.Log;

public class CategorySearchRemoveCommonWordVisitor implements CategoryVisitor{
	private static final double MIN_PERCENT_MATCHING = 0.1;

	private static final int MAX_VALUE_RIGHT_ORDER = 20;

	public static int NUM_MAX_OUTPUT_DEFAULT = 10;
	public static int NUM_MAX_OUTPUT = NUM_MAX_OUTPUT_DEFAULT;
	private static final String TAG = "ParseStatistic";
	List<MySentence> mDatabase_get_rightList;
	boolean mFlagRightMaxValue;
	public String INPUT_FILE_DATABASE = "database.txt";
	String mKey;

	public CategorySearchRemoveCommonWordVisitor(String key) {
		mKey = key;
	}

	void calculateMatchingPercentSentence(List<MySentence> database,
			String input) {
		String[] keywords = take_keyword(input);
		// check keywords whether ok for search
		if (keywords.length < 1)
			return;
		
		
		// check input if tieng viet thi check xem tieng viet co dau hay khong dau
		int valueLangSearch = Constant.getLANGMODE_SEARCH().value;
		if (Constant.getLANGMODE_INPUT() == Language.VIET) {
			// check whether signed or not
			if (ConvertUnsigned.isSignedString(input))
				valueLangSearch = Constant.getLANGMODE_INPUT().value;
			else
				valueLangSearch = Constant.getLANGMODE_SEARCH().value;
		}
		
		
		// tim % cua tung cau
		float resultOf1Matched;
		mFlagRightMaxValue = false;
		mDatabase_get_rightList = new ArrayList<MySentence>();

		// check
		for (int i = 0; i < database.size(); i++) {
			resultOf1Matched = 0;
			// check neu cai nay dung' toan ven chu~ va order
			MySentence sentence = database.get(i);
			if (sentence.getmText()[valueLangSearch]
					.toLowerCase().contains(input.toLowerCase())) {
				// neu co cau nao > max value thi chi show nhung cau do ra thoi
				// flag cau nay la max value va chi tinh % cau nay tro di
				mFlagRightMaxValue = true;
				sentence.setmPercentMatch(keywords.length
						/ sentence.getAmountWord(Constant.getLANGMODE_INPUT()) + MAX_VALUE_RIGHT_ORDER);
				mDatabase_get_rightList.add(sentence);
				continue;
			}
			if (mFlagRightMaxValue) {
				sentence.setmPercentMatch(0);
				continue;
			}
			// end check match full sentence
			
			// 2 kinds of keyword: the end for contain, the first part for word
			int sizeKeywordWord = keywords.length;
			
			// check keyword contain
			if (input.charAt(input.length()-1) != ' ') {
				String keyword = keywords[keywords.length-1];
				sizeKeywordWord--;
				if (Pattern
				.compile(Pattern.quote(keyword),
						Pattern.CASE_INSENSITIVE | Pattern.UNICODE_CASE)
				.matcher(
						sentence.getmText()[valueLangSearch]).find())
				resultOf1Matched++;
			}
			
			// check keyword whole word	
			if (sizeKeywordWord > 0) {
				String join = keywords[0];
				for (int index = 1; index<sizeKeywordWord; index++) {
					join += "|" + keywords[index];
				}
				// TODO: chua xet vu order cua cac token
				// version 2: take each word
				String patternString = "\\b(" + join + ")\\b";
				Pattern pattern = Pattern.compile(patternString, Pattern.CASE_INSENSITIVE | Pattern.UNICODE_CASE);
				Matcher matcher = pattern.matcher(sentence.getmText()[valueLangSearch]);
	
				while (matcher.find()) {
	//			    System.out.println(matcher.group(1));
				    resultOf1Matched++;
				}
			}

			
			sentence.setmPercentMatch(resultOf1Matched + resultOf1Matched
					/ sentence.getAmountWord(Constant.getLANGMODE_INPUT()));

		}

	}

	List<MySentence> getSentenceInCate(Category rootCate,
			List<MySentence> initSentences) {
		for (Item item : rootCate.getmItems()) {
			if (item instanceof Category) {
				getSentenceInCate((Category) item, initSentences);
			} else if (item instanceof MySentence) {
				initSentences.add((MySentence) item);
			}
		}

		return initSentences;
	}

	List<MySentence> getTopSentences(List<MySentence> database) {
		List<MySentence> results = new ArrayList<MySentence>();
		int size = database.size();
		if (size > NUM_MAX_OUTPUT)
			size = NUM_MAX_OUTPUT;
		for (int i = 0; i < size; i++) {
			Log.d("test", "phantram matched = "
					+ database.get(i).getmPercentMatch());
			results.add(database.get(i));
		}
		// remove duplicate sentences
		for (int i = 0; i < results.size() - 1; i++) {
			for (int j = i + 1; j < results.size(); j++) {
				if (results.get(i).getmText()[Constant.getLANGMODE_INPUT().value]
						.equals(results.get(j).getmText()[Constant
								.getLANGMODE_INPUT().value])) {
					results.remove(j);
					j--;
				}
			}
		}
		return results;
	}

	public List<MySentence> parse(List<MySentence> database, String input) {
		// TODO: if want to improve, so can get optimize keyword
		calculateMatchingPercentSentence(database, input);
		// neu ma co flag chi show cac cau dung' order thi show ra
		if (mFlagRightMaxValue) {
			return sortAlgorithmRecommend(mDatabase_get_rightList);
		}
		List<MySentence> take_database = sortAlgorithmRecommend(database);

		// remove sentence less than % matching
		take_database = removeSentenceFailed(take_database,
				MIN_PERCENT_MATCHING);

		return take_database;
	}

	public List<MySentence> removeSentenceFailed(List<MySentence> sentences,
			double percentMatchingMin) {
		List<MySentence> results = new ArrayList<MySentence>();
		for (MySentence s : sentences) {
			if (s.getmPercentMatch() >= percentMatchingMin)
				results.add(s);
		}
		return results;
	}

	public List<MySentence> search_category(String key, String input,
			Language lang) {
		input = Constant.pre_processing_input(input);
		List<MySentence> lm = new ArrayList<MySentence>();
		return parse(lm, input);
	}

	private List<MySentence> sortAlgorithmRecommend(List<MySentence> database) {
		// sort count truoc, roi sort %
//		sortPercentMatched(database);
//		List<MySentence> take_database = getTopSentences(database);
//		sortByCount(take_database);
//		sortPercentMatched(take_database);
		
		sortByCount(database);
		sortPercentMatched(database);
		return database;
	}

	void sortByCount(List<MySentence> database) {
		Collections.sort(database, new SentenceCountComparator());
		Log.d("test", "finish sort by attribute count");
	}

	void sortPercentMatched(List<MySentence> database) {
		Collections.sort(database, new SentenceComparator());
		Log.d("test", "finish sort by attribute percent matched");
	}

	//
	String[] take_keyword(String input) {
		return input.split(" ");
	}

	public List<MySentence> visit(Category rootCate) {

		// version 1:
		/*
		 * if key == "" => show basic sentences lay cac cau ra roi sort va
		 * search nhu cu~
		 */

		if (mKey.equals("")) {
			// show basic sentences
			List<MySentence> sentences = rootCate.getSentences();
			sortByCount(sentences);
//			return getTopSentences(sentences);
			return sentences;
		}
		List<MySentence> sentences = getSentenceInCate(rootCate,
				new ArrayList<MySentence>());
		return parse(sentences, mKey);
	}

}
