package model.visitor;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import model.Category;
import model.Item;
import model.MySentence;
import model.MySentenceDecorator;
import model.SentenceComparator;
import model.SentenceCountComparator;
import model.supporter.MarkTag;
import thesis.travelspeakbook.current_data.Constant;
import android.util.Log;

public class CategorySearchVisitor implements CategoryVisitor {
	private static final double MIN_PERCENT_MATCHING = 0.1;

	private static final float MAX_VALUE_RIGHT_ORDER = 20f;

	public static int NUM_MAX_OUTPUT_DEFAULT = 10;
	public static int NUM_MAX_OUTPUT = NUM_MAX_OUTPUT_DEFAULT;
	private static final String TAG = "ParseStatistic";
	List<MySentence> mDatabase_get_rightList;
	boolean mFlagRightMaxValue;
	public String INPUT_FILE_DATABASE = "database.txt";
	String mKey;

	public CategorySearchVisitor(String key) {
		mKey = Constant.pre_processing_input(key);
	}

	String makeDump(String text) {
		String result = "";
		int l = text.length();
		for (int i = 0; i < l; i++) {
			result += ";";
		}
		return result;
	}

	static float resultMatched = 0;

	String searchAWord(String word, String text, MySentenceDecorator sentence, boolean isByEachWord) {
		String wordPattern = "";
		if (isByEachWord) {
			wordPattern = "\\b" + word + "\\b";
		}
		else 
			wordPattern = word;
		
		Pattern pattern = Pattern.compile(wordPattern, Pattern.CASE_INSENSITIVE);
		Matcher matcher = pattern.matcher(text);

		StringBuffer sb = new StringBuffer();
		while (matcher.find()) {
			matcher.appendReplacement(sb, makeDump(word));
			addTagHighlightMatching(sentence, matcher);
			resultMatched++;
			break;
		}
		matcher.appendTail(sb);
		return sb.toString();
	}

	private void addTagHighlightMatching(MySentenceDecorator sentence, Matcher matcher) {
		sentence.addMarkTag(new MarkTag(matcher.start(),
				Constant.BOLD_HEADER));
		sentence.addMarkTag(new MarkTag(matcher.end(),
				Constant.BOLD_TAIL));
	}

	void calculateMatchingPercentSentence(List<MySentence> database,
			String input) {
		String [] keywords = take_keyword( input );
        // check keywords whether ok for search
        if (keywords . length < 1 )
            return ;
       
       
        int valueLangSearch = Constant .getValueLangSearch ( input);
       
       
        // tim % cua tung cau
        mFlagRightMaxValue = false ;
        mDatabase_get_rightList = new ArrayList <MySentence>();

        // check
        for (int i = 0 ; i < database. size (); i ++) {
            resultMatched = 0 ;
            // check neu cai nay dung' toan ven chu~ va order
            MySentenceDecorator sentence = new MySentenceDecorator(database.get(i));
            
            Pattern pt = Pattern.compile(input, Pattern.CASE_INSENSITIVE);
            Matcher mc = pt.matcher(sentence .getmText()[valueLangSearch]);
            if (mc.find()) {
                // neu co cau nao > max value thi chi show nhung cau do ra thoi
                // flag cau nay la max value va chi tinh % cau nay tro di
                mFlagRightMaxValue = true ;
                sentence.setmPercentMatch (MAX_VALUE_RIGHT_ORDER + (float)(keywords.length)
                        / sentence.getAmountWord (Constant.getLANGMODE_INPUT()));
                mDatabase_get_rightList.add(sentence);
                
                addTagHighlightMatching(sentence, mc);
                continue ;
           }
            if (mFlagRightMaxValue) {
                sentence.setmPercentMatch(0);
                continue;
           }
            // end check match full sentence
           
            // 2 kinds of keyword: the end for contain, the first part for word
            int sizeKeywordWord = keywords.length ;
            String text = sentence .getmText()[valueLangSearch];
            for (int j = 0 ;j < sizeKeywordWord- 1; j++) {
                text = searchAWord(keywords [j], text, sentence, true );
           }
            text = searchAWord(keywords [ sizeKeywordWord- 1 ], text , sentence, false );
           
           
            sentence.setmPercentMatch(resultMatched + resultMatched
                   / sentence .getAmountWord(Constant.getLANGMODE_INPUT()));
            
            
            database.set(i, sentence);
       }
	}

	List<MySentence> getSentenceInCate(Category rootCate,
			List<MySentence> initSentences) {
		for (Item item : rootCate.getmItems()) {
			if (item instanceof Category) {
				getSentenceInCate((Category) item, initSentences);
			} else if (item instanceof MySentence) {
				initSentences.add((MySentence) item);
			}
		}

		return initSentences;
	}

	List<MySentence> getTopSentences(List<MySentence> database) {
		List<MySentence> results = new ArrayList<MySentence>();
		int size = database.size();
		if (size > NUM_MAX_OUTPUT)
			size = NUM_MAX_OUTPUT;
		for (int i = 0; i < size; i++) {
			Log.d("test", "phantram matched = "
					+ database.get(i).getmPercentMatch());
			results.add(database.get(i));
		}
		// remove duplicate sentences
		for (int i = 0; i < results.size() - 1; i++) {
			for (int j = i + 1; j < results.size(); j++) {
				if (results.get(i).getmText()[Constant.getLANGMODE_INPUT().value]
						.equals(results.get(j).getmText()[Constant
								.getLANGMODE_INPUT().value])) {
					results.remove(j);
					j--;
				}
			}
		}
		return results;
	}

	public List<MySentence> parse(List<MySentence> database, String input) {
		// TODO: if want to improve, so can get optimize keyword
		calculateMatchingPercentSentence(database, input);
		// neu ma co flag chi show cac cau dung' order thi show ra
		if (mFlagRightMaxValue) {
			List<MySentence> take_database = sortAlgorithmRecommend(mDatabase_get_rightList);
			take_database = removeDuplicateTextSentence(take_database);
			return take_database;
		}
		List<MySentence> take_database = sortAlgorithmRecommend(database);

		// remove sentence less than % matching
		// take above MAN 50%
		if (take_database.size() > 0) {
			float percentMin = take_database.get(0).getmPercentMatch()*Constant.THRESHOLD_MIN_PERCENT;
//			take_database = removeSentenceFailed(take_database,
//					MIN_PERCENT_MATCHING);
			take_database = removeSentenceFailed(take_database,
					percentMin);
			
			// remove duplicate sentence
			take_database = removeDuplicateTextSentence(take_database);
		}

		return take_database;
	}

	List<MySentence> removeDuplicateTextSentence(List<MySentence> sentences) {
		int size = sentences.size();
		for (int i = 0; i < size - 1; i++) {
			for (int j = i + 1; j < size; j++) {
				String text_a = sentences.get(i).getmText()[Constant
						.getLANGMODE_INPUT().value];
				String text_b = sentences.get(j).getmText()[Constant
						.getLANGMODE_INPUT().value];
				if (text_a.equals(text_b)) {
					sentences.remove(j);
					j--;
					size--;
				}
			}
		}
		return sentences;
	}

	public List<MySentence> removeSentenceFailed(List<MySentence> sentences,
			double percentMatchingMin) {
		List<MySentence> results = new ArrayList<MySentence>();
		for (MySentence s : sentences) {
			if (s.getmPercentMatch() >= percentMatchingMin)
				results.add(s);
		}
		return results;
	}

	// public List<MySentence> search_category(String key, String input,
	// Language lang) {
	// input = Constant.pre_processing_input(input);
	// List<MySentence> lm = new ArrayList<MySentence>();
	// return parse(lm, input);
	// }

	private List<MySentence> sortAlgorithmRecommend(List<MySentence> database) {
		// sort count truoc, roi sort %
		// sortPercentMatched(database);
		// List<MySentence> take_database = getTopSentences(database);
		// sortByCount(take_database);
		// sortPercentMatched(take_database);

		sortByCount(database);
		sortPercentMatched(database);
		return database;
	}

	void sortByCount(List<MySentence> database) {
		Collections.sort(database, new SentenceCountComparator());
		Log.d("test", "finish sort by attribute count");
	}

	void sortPercentMatched(List<MySentence> database) {
		Collections.sort(database, new SentenceComparator());
		Log.d("test", "finish sort by attribute percent matched");
	}

	//
	String[] take_keyword(String input) {
		return Pattern.compile("(\\s)+").split(input);
	}

	public List<MySentence> visit(Category rootCate) {

		// version 1:
		/*
		 * if key == "" => show basic sentences lay cac cau ra roi sort va
		 * search nhu cu~
		 */

		if (mKey.equals("")) {
			// show basic sentences
			List<MySentence> sentences = rootCate.getSentences();
			sortByCount(sentences);
			// return getTopSentences(sentences);
			return sentences;
		}
		List<MySentence> sentences = getSentenceInCate(rootCate,
				new ArrayList<MySentence>());
		return parse(sentences, mKey);
	}
}
