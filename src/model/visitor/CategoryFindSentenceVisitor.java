package model.visitor;

import java.util.List;

import thesis.travelspeakbook.current_data.Constant;

import model.Category;
import model.Item;
import model.MySentence;
import model.PatternSentence;

public class CategoryFindSentenceVisitor implements CategoryVisitor {
	PatternSentence mPatternSentece;
	List<Item> mItemsSelectedInTagChoose;
	
	public CategoryFindSentenceVisitor(PatternSentence ps, List<Item> itemsSelectedInTagChoose) {
		mPatternSentece = ps;
		mItemsSelectedInTagChoose = itemsSelectedInTagChoose;
	}
	
	public Object visit(Category c) {
		return findSentence(c, mPatternSentece.getmText()[0]);
		
	}
	Object findSentence(Category c, String textPattern) {
		for (int i = 0; i<c.getmItems().size(); i++) {
			Item item = c.getmItems().get(i);
			MySentence s = (MySentence) item;
			PatternSentence ps = s.getmPattern();
			if (ps == null) {
				continue;
			}
			if (ps == mPatternSentece) {
				// check indexes
				if (isEqual2ListInteger(s.getmTagChooseSelectedItems(), mItemsSelectedInTagChoose)) {
					return s;
				}
			}
			
		}
		return null;
	}
	boolean isEqual2ListInteger(List<Item> l1, List<Item> l2) {
		int valueLangTest = Constant.getLANGMODE_INPUT().value;
		int s1 = l1.size();
		int s2 = l2.size();
		if (s1 != s2) 
			return false;
		for (int i = 0; i<s1; i++) {
			if (!l1.get(i).getmText()[valueLangTest].equals(l2.get(i).getmText()[valueLangTest]))
				return false;
		}
		return true;
	}

}
