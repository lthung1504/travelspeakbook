package model;

import java.util.Comparator;

public class SentenceCountComparator implements Comparator<MySentence> {
	public int compare(MySentence s1, MySentence s2) {
		int compare;
		if (s1.mCountSelected < s2.mCountSelected)
			compare = 1;
		else if (s1.mCountSelected > s2.mCountSelected)
			compare = -1;
		else
			compare = 0;
		return compare;
	}
}