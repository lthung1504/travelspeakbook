package model;


import java.util.ArrayList;
import java.util.List;

import model.supporter.MarkTag;

public class MySentenceDecorator extends MySentence {
	public MySentenceDecorator(MySentence sentence) {
		mSentence = sentence;
		mMarkTags = new ArrayList<MarkTag>();
	}
	MySentence mSentence;
	List<MarkTag> mMarkTags;
	
	public void addMarkTag(MarkTag mt) {
		mMarkTags.add(mt);
	}
	public void removeAllMarkTag() {
		mMarkTags.clear();
	}
	public List<MarkTag> getmMarkTags() {
		return mMarkTags;
	}
	public void setmMarkTags(List<MarkTag> mMarkTags) {
		this.mMarkTags = mMarkTags;
	}
	public int getAmountWord(Language l) {
		return mSentence.getAmountWord(l);
	}
	public String getContent() {
		return mSentence.getContent();
	}
	public Category getmBelongCate() {
		return mSentence.getmBelongCate();
	}
	public int getmCountSelected() {
		return mSentence.getmCountSelected();
	}
	public PatternSentence getmPattern() {
		return mSentence.getmPattern();
	}
	public float getmPercentMatch() {
		return mSentence.getmPercentMatch();
	}
	public List<Item> getmTagChooseSelectedItems() {
		return mSentence.getmTagChooseSelectedItems();
	}
	public List<Item> getmTagOptionalSelectedItems() {
		return mSentence.getmTagOptionalSelectedItems();
	}
	
	public String[] getmText() {
		return mSentence.getmText();
	}
	
	public void increaseCount() {
		mSentence.increaseCount();
	}
	public boolean ismIsFavorite() {
		return mSentence.ismIsFavorite();
	}
	public void refreshData() {
		mSentence.refreshData();
	}
	public void setCount(int a) {
		mSentence.setCount(a);
	}
	public void setmBelongCate(Category category) {
		mSentence.setmBelongCate(category);
	}
	public void setmCountSelected(int a) {
		mSentence.setmCountSelected(a);
	}
	public void setmIsFavorite(boolean a) {
		mSentence.setmIsFavorite(a);
	}
	public void setmPattern(PatternSentence a) {
		mSentence.setmPattern(a);
	}
	public void setmPercentMatch(float a) {
		mSentence.setmPercentMatch(a);
	}
	public void setmTagChooseSelectedItems(List<Item> a) {
		mSentence.setmTagChooseSelectedItems(a);
	}
	public void setmTagOptionalSelectedItems(List<Item> a) {
		mSentence.setmTagOptionalSelectedItems(a);
	}
}
