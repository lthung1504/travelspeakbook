package model;

import java.io.Serializable;
import java.util.List;

import model.Item.Language;

public class SemiPattern implements Serializable{
	Language mLang;
	List<PositionTag> mPosTags;
	List<PositionTag> mPosTagsManualFill;
	public SemiPattern(Language lang, List<PositionTag> posTags, List<PositionTag> posTagsManual) {
		mLang = lang;
		mPosTags = posTags;
		mPosTagsManualFill = posTagsManual;
	}
	
	/**
	 * @return the mLang
	 */
	public Language getmLang() {
		return mLang;
	}
	/**
	 * @return the mPosTags
	 */
	public List<PositionTag> getmPosTags() {
		return mPosTags;
	}
	/**
	 * @param mLang the mLang to set
	 */
	public void setmLang(Language mLang) {
		this.mLang = mLang;
	}
	/**
	 * @param mPosTags the mPosTags to set
	 */
	public void setmPosTags(List<PositionTag> mPosTags) {
		this.mPosTags = mPosTags;
	}

	/**
	 * @return the mPosTagsManualFill
	 */
	public List<PositionTag> getmPosTagsManualFill() {
		return mPosTagsManualFill;
	}

	/**
	 * @param mPosTagsManualFill the mPosTagsManualFill to set
	 */
	public void setmPosTagsManualFill(List<PositionTag> mPosTagsManualFill) {
		this.mPosTagsManualFill = mPosTagsManualFill;
	}
	
	
	
}
