package model;

import java.util.Comparator;

public class TagPosComparator implements Comparator<PositionTag> {
	public int compare(PositionTag s1, PositionTag s2) {
		int compare;
		if (s1.getmPos() > s2.getmPos())
			compare = 1;
		else if (s1.getmPos() < s2.getmPos())
			compare = -1;
		else
			compare = 0;
		return compare;
	}
}
