package model;

import java.io.Serializable;
import java.util.List;

public class TagSentence implements Serializable {
	String mKey;
	String mSign;
	List<Item> mItems;
	
	boolean mIsTagOptional;
	public TagSentence(String key, List<Item> items, String sign, boolean isTagOptional) {
		mKey = key;
		mItems = items;
		mSign = sign;
		mIsTagOptional = isTagOptional;
	}
	public Item chooseTag(int index) { 
		return mItems.get(index);
	}
	public String toString() {
		if (mItems != null)
			return "[" + mKey + "," + mItems.toString() + "]";
		return mKey;
	}
	public boolean isTagOptional() {
		if (mItems == null) 
			return true;
		return false;
	}
	/**
	 * @return the mKey
	 */
	public String getmKey() {
		return mKey;
	}
	/**
	 * @param mKey the mKey to set
	 */
	public void setmKey(String mKey) {
		this.mKey = mKey;
	}
	/**
	 * @return the mItems
	 */
	public List<Item> getmItems() {
		return mItems;
	}
	/**
	 * @param mItems the mItems to set
	 */
	public void setmItems(List<Item> mItems) {
		this.mItems = mItems;
	}
	/**
	 * @return the mSign
	 */
	public String getmSign() {
		return mSign;
	}
	/**
	 * @param mSign the mSign to set
	 */
	public void setmSign(String mSign) {
		this.mSign = mSign;
	}
	public boolean ismIsTagOptional() {
		return mIsTagOptional;
	}
	public void setmIsTagOptional(boolean mIsTagOptional) {
		this.mIsTagOptional = mIsTagOptional;
	}
}
