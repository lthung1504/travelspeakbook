package model;


import java.util.ArrayList;
import java.util.List;

import model.visitor.CategoryVisitor;


public class Category extends Item{
	List<Item> mItems = null;
	String mLevel;
	int mIdIcon;
	Category mBelongCate;
	
	public Category(String name, Category belongCate) {
		this(name, name, name, name, belongCate);
	}
	public Category(String nameEng, String nameViet, String nameLao, String nameCamp, Category belongCate) {
		super(new String[] {nameEng, nameViet, nameLao, nameCamp});
		mItems = new ArrayList<Item>();
		mLevel = "0";
		mBelongCate = belongCate;
	}
	public Category(String[] names, Category belongCate) {
		this(names[0], names[1], names[2], names[3], belongCate);
	}
	
	public boolean add(Item s) {
		mItems.add(s);
		return true;
	}
	public Item get(int location) {
		return mItems.get(location);
	}
	
	public Category getCateInsideThisLevel(String nameCate, int positionLang) {
		if (!isHaveInnerCate())
			return null;
		for (int i = 0; i<mItems.size(); i++) {
			if (mItems.get(i).equal(nameCate, positionLang)) {
				return (Category)mItems.get(i);
			}
		}
		return null;
	}
	private boolean isHaveInnerCate() {
		if (mItems.size() == 0) 
			return false;
		if (mItems.get(0) instanceof MySentence) 
			return false;
		return true;
	}
	
	public List<MySentence> getSentences() {
		return getSentences(mItems);
	}
	public List<MySentence> getSentences(List<Item> items) {
		List<MySentence> results = new ArrayList<MySentence>();
		// di sau vao cac context
		for (Item item : items) {
			if (item instanceof MySentence) {
				results.add((MySentence)item);
			}
			else if (item instanceof Category) {
				results.addAll(getSentences(((Category) item).getmItems()));
			}
		}
		return results;
	}
	
	public Object accept(CategoryVisitor visitor) {
		return visitor.visit(this);
	}
	/**
	 * @return the mItems
	 */
	public List<Item> getmItems() {
		return mItems;
	}
	/**
	 * @param mItems the mItems to set
	 */
	public void setmItems(List<Item> mItems) {
		this.mItems = mItems;
	}
	/**
	 * @return the mLevel
	 */
	public String getmLevel() {
		return mLevel;
	}
	/**
	 * @param mLevel the mLevel to set
	 */
	public void setmLevel(String mLevel) {
		this.mLevel = mLevel;
	}
	
	public String getText(int positionLang) { 
		return mText[positionLang];
	}
	/**
	 * @return the mIdIcon
	 */
	public int getmIdIcon() {
		return mIdIcon;
	}
	/**
	 * @param mIdIcon the mIdIcon to set
	 */
	public void setmIdIcon(int mIdIcon) {
		this.mIdIcon = mIdIcon;
	}
	public int size() {
		return mItems.size();
	}
	public Category getmBelongCate() {
		return mBelongCate;
	}
	public void setmBelongCate(Category mBelongCate) {
		this.mBelongCate = mBelongCate;
	}
	
}
