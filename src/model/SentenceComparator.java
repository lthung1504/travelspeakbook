package model;

import java.util.Comparator;


public class SentenceComparator implements Comparator<MySentence> {
	public int compare(MySentence s1, MySentence s2) {
		int compare;
		if (s1.mPercentMatch < s2.mPercentMatch)
			compare = 1;
		else if (s1.mPercentMatch > s2.mPercentMatch)
			compare = -1;
		else
			compare = 0;
		return compare;
		// return (s1.phantramMatch <= s2.phantramMatch) ? 1 : -1;
	}
}

