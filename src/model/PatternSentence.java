package model;



public class PatternSentence extends Item{
	SemiPattern[] mSemiPatterns; // ket hop voi Item, la phan nho chu' khong co' text

	/**
	 * @return the mSemiPatterns
	 */
	public SemiPattern[] getmSemiPatterns() {
		return mSemiPatterns;
	}
	/**
	 * @param mSemiPatterns the mSemiPatterns to set
	 */
	public void setmSemiPatterns(SemiPattern[] mSemiPatterns) {
		this.mSemiPatterns = mSemiPatterns;
	}
	public void setmSemiPatterns(SemiPattern semi, int lang) {
		mSemiPatterns[lang] = semi;
	}
	public PatternSentence(String text_eng, String text_viet, String text_lao, String text_campuchia) {
		super(new String[] {text_eng, text_viet, text_lao, text_campuchia});
		mSemiPatterns = new SemiPattern[TOTAL_LANGS];
	}

	public PatternSentence() {
		super();
		mSemiPatterns = new SemiPattern[TOTAL_LANGS];
	}
	public void fixTextEng(String text_eng) {
		mText[0] = text_eng;
	}
	
	
}
