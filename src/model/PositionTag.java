package model;

import java.io.Serializable;


public class PositionTag implements Serializable {
	TagSentence mTag;
	int mPos;
	public PositionTag(TagSentence tag, int pos) {
		mTag = tag;
		mPos = pos;
	}
	/**
	 * @return the mPos
	 */
	public int getmPos() {
		return mPos;
	}
	/**
	 * @param mPos the mPos to set
	 */
	public void setmPos(int mPos) {
		this.mPos = mPos;
	}
	/**
	 * @return the mTag
	 */
	public TagSentence getmTag() {
		return mTag;
	}
	/**
	 * @param mTag the mTag to set
	 */
	public void setmTag(TagSentence mTag) {
		this.mTag = mTag;
	}
	
}
