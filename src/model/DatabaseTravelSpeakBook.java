package model;

import java.io.Serializable;

public class DatabaseTravelSpeakBook implements Serializable {
	Category mRootCate;
	Category mFavoriteCate;
	public DatabaseTravelSpeakBook(Category rootCate, Category favoriteCate) {
		mRootCate = rootCate;
		mFavoriteCate = favoriteCate;
	}
	public Category getmRootCate() {
		return mRootCate;
	}
	public void setmRootCate(Category mRootCate) {
		this.mRootCate = mRootCate;
	}
	public Category getmFavoriteCate() {
		return mFavoriteCate;
	}
	public void setmFavoriteCate(Category mFavoriteCate) {
		this.mFavoriteCate = mFavoriteCate;
	}
	
}