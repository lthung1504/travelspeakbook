package model;

import java.util.List;



public class MySentence extends Item{
	private static final String REGEX_SIGN_SPECIAL_IN_SENTENCE = "\\?|!";
	float mPercentMatch = 0;
	public Category getmBelongCate() {
		return mBelongCate;
	}



	public void setmBelongCate(Category mBelongCate) {
		this.mBelongCate = mBelongCate;
	}


	int mCountSelected = 0;
	boolean mIsFavorite = false;
	PatternSentence mPattern;
	Category mBelongCate;
	
	List<Item> mTagChooseSelectedItems;
	List<Item> mTagOptionalSelectedItems;
	
	public MySentence() {
		
	}
	public MySentence(String text_eng, String text_viet, String text_lao, String text_campuchia, List<Item> posChooseTags, List<Item> posOptionalTags, Category belongCate) {
		super(new String[] {text_eng, text_viet, text_lao, text_campuchia});
		mTagChooseSelectedItems = posChooseTags;
		mTagOptionalSelectedItems = posOptionalTags;
		mBelongCate = belongCate;
	}
	
	public void refreshData() {
		// lay textpattern roi dien tung cai vao
		String[] textPattern = mPattern.getmText();
		List<PositionTag> tags1 = mPattern.getmSemiPatterns()[0].getmPosTags();
		List<PositionTag> tags2 = mPattern.getmSemiPatterns()[0].getmPosTagsManualFill();
		
		for (int i = 0; i<TOTAL_LANGS; i++ ) {
			String text = textPattern[i];
			// thay tung tag
			for (int j = 0; j<tags1.size(); j++) {
				PositionTag pt = tags1.get(j);
				String sign = pt.getmTag().getmSign();
				String textChange = mTagChooseSelectedItems.get(j).getmText()[i];
				text = text.replace(sign, textChange);
			}
			for (int j = 0; j<tags2.size(); j++) {
				PositionTag pt = tags2.get(j);
				String sign = pt.getmTag().getmSign();
				String textChange = mTagOptionalSelectedItems.get(j).getmText()[i];
				text = text.replace(sign, textChange);
			}	
			mText[i] = text;
		}
		ConvertUnsigned cu = new ConvertUnsigned();
		mText[Language.VIET_UNSIGNED.value] = cu.ConvertString(mText[Language.VIET.value]);
	}

	// for testing
	public String getContent() {
		return this.mText[Language.ENG.value] + "\t" + this.mPercentMatch;
	}
	public int getAmountWord(Language lang) {
		String[] words = mText[lang.value].split(" ");
		int length = words.length;
		if (words[length-1].matches(REGEX_SIGN_SPECIAL_IN_SENTENCE)) {
			length--;
		}
		return length;
	}
	public void setCount(int count) {
		mCountSelected = count;
	}
	/**
	 * @return the mPercentMatch
	 */
	public float getmPercentMatch() {
		return mPercentMatch;
	}
	/**
	 * @param mPercentMatch the mPercentMatch to set
	 */
	public void setmPercentMatch(float mPercentMatch) {
		this.mPercentMatch = mPercentMatch;
	}
	/**
	 * @return the mCountSelected
	 */
	public int getmCountSelected() {
		return mCountSelected;
	}
	/**
	 * @param mCountSelected the mCountSelected to set
	 */
	public void setmCountSelected(int mCountSelected) {
		this.mCountSelected = mCountSelected;
	}
	/**
	 * @return the mIsFavorite
	 */
	public boolean ismIsFavorite() {
		return mIsFavorite;
	}
	/**
	 * @param mIsFavorite the mIsFavorite to set
	 */
	public void setmIsFavorite(boolean mIsFavorite) {
		this.mIsFavorite = mIsFavorite;
	}
	/**
	 * @return the mPattern
	 */
	public PatternSentence getmPattern() {
		return mPattern;
	}
	/**
	 * @param mPattern the mPattern to set
	 */
	public void setmPattern(PatternSentence mPattern) {
		this.mPattern = mPattern;
	}
	public void increaseCount() {
		mCountSelected++;
	}



	public List<Item> getmTagChooseSelectedItems() {
		return mTagChooseSelectedItems;
	}



	public void setmTagChooseSelectedItems(List<Item> mTagChooseSelectedItems) {
		this.mTagChooseSelectedItems = mTagChooseSelectedItems;
	}



	public List<Item> getmTagOptionalSelectedItems() {
		return mTagOptionalSelectedItems;
	}



	public void setmTagOptionalSelectedItems(List<Item> mTagOptionalSelectedItems) {
		this.mTagOptionalSelectedItems = mTagOptionalSelectedItems;
	}










}

